def splits(word):
    """
    returns a list of all posible splits of word
    include the empty splits
    returns a list of tuples.
    >>> splits('ana')
    [('', 'ana'), ('a', 'na'), ('an', 'a'), ('ana', '')]
    """

    rlist = []
    rlist.append(('',word))

    for c in word:
        (w,w2) = word.split(c,1)
        if (w,w2) not in rlist:
            w+=c
        else:
            w2 = c+w2
        if (w,w2) not in rlist:
            rlist.append((w,w2))

    rlist.append((word,''))
    return rlist



def deletes(word):
    """
    all misspellings of word caused by omitting a letter
    use the splits function
    >>> 'rockets' not in deletes('rocket')
    True
    >>> 'roket' in deletes('rocket')
    True
    """
    l = []
    for c in word:
        w,w2 = word.split(c,1)
        l.append(w+w2)
    return l

def transposes(word):
    l = list(word)
    rlist = []
    for i in range(0,len(l)-1,1):
        l[i],l[i+1]=l[i+1],l[i]
        string=''.join(str(e) for e in l)
        rlist.append(string)
        l[i],l[i+1]=l[i+1],l[i]
    return rlist

def substitutions(word):
    pass

print(transposes("paranormal"))
