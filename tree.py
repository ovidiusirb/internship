from __future__ import  print_function

import os

def tree(file,string,cnt):
    if string == "":
        print("Tree of {}".format(file))
        e = file
    else:
        e = string

    if os.path.isdir(e):
        for f in sorted(os.listdir(e)):
            e2 = os.path.join(e, f)
            if os.path.isdir(e2):
                print("|->" + e2)
                print(cnt*"    ", end='')
                cnt += 1
                tree(f,e2,cnt)
                cnt = 1
            else:
                print("|->" + e2)
                tree(f,e2,cnt)


tree("/home/ovi/my-project","",1)
