# Exercises for notions learned in unit 1
# ---------------------------------------

# takes a message, capitalizes the first letter then underlines the message with =
# print make_title('ana')
# Ana
# ===
# use the string method upper
def make_title(message):
    title = ""
    title+=message[0].upper()
    r = range(1,len(message),1)
    for i in r:
        title+=message[i]
    title+='\n---'
    return title

# given a list of tuples of length 2 it returns a list of the tuples inverted
# if both values in the tuple are the same then we omit them from the output
# [(2, 4), (3, 3), (1, 2)] will become [(4, 2), (2, 1)]
def invert_tuples(lst):
    newlst=[]
    for tuple in lst:
        if tuple[0]!=tuple[1]:
            tuple2 = tuple[1],tuple[0]
            newlst.append(tuple2)
    return newlst

# curses is a dictionary of cursewords to censored curses
# Given a list of words replace the curses according to the dict
# censor(['you', 'fucker'], {'fucker': 'f**er'}) should return
# ['you', 'f**er']
def censor(lst, curses):
    rlst=[]
    for word in lst:
        if word in curses.keys():
            rlst.append(curses[word])
        else:
            rlst.append(word)
    return rlst

# Given a string s, return a string made of the first 2
# and the last 2 chars of the original string,
# so 'spring' yields 'spng'. However, if the string length
# is less than 2, return instead the empty string.
def both_ends(s):
    try:
        string = s[0]+s[1]+s[-2]+s[-1]
        return string
    except IndexError:
        return ''


# return the factorial of n
# factorial(5) = 1*2*3*4*5
def factorial(n):
    r = range(1,n+1,1)
    res = 1
    for i in r:
        res*=i
    return res


# return a list of fibonacci numbers smaller than n
def fibo(n):
    l = [0]
    f1 = 1
    f2 = 1
    l.append(f1)
    l.append(f2)
    while f2 < n:
        f3=f1+f2
        f1 = f2
        f2 = f3
        if f2 >= n:
            break
        l.append(f2)
    return l


def test_make_title():
    assert make_title('ana') == 'Ana\n==='

def test_invert_tuples():
    assert invert_tuples([(2, 4), (3, 3), (1, 2)]) == [(4, 2), (2, 1)]

def test_censor():
    assert censor(['you', 'fucker'], {'fucker': 'f**er'}) == ['you', 'f**er']

def test_both_ends():
    assert both_ends('spring') == 'spng'
    assert both_ends('Hello') == 'Helo'
    assert both_ends('a') == ''
    assert both_ends('xyz') == 'xyyz'

def test_factorial():
    assert factorial(5) == 1*2*3*4*5
    assert factorial(1) == 1
    assert factorial(0) == 1

