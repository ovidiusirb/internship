from bs4 import BeautifulSoup
import urllib2

def getUrl():
    url = "http://www.hotnews.ro//"
    name = urllib2.urlopen(url)
    soup = BeautifulSoup(name,'html.parser')
    return soup

def getLinks():
    l = []
    soup = getUrl()
    for link in soup.find_all('a'):
        if 'dna' in link.get('href').lower():
            l.append(link.get('href'))
    return l

def getTitles():
    titles=[]

    l = getLinks()
    for link in l:
        name = urllib2.urlopen(link)
        soup = BeautifulSoup(name,'html.parser')
        if soup.title.string not in titles:
            titles.append(str(soup.title.string))
    return titles

def getLastTwoArticles():
    titles = getTitles()
    articles = []
    for article in titles[:2]:
        articles.append(article)
    return articles

print(getLastTwoArticles())