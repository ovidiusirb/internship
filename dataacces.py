import sqlite3
import sys
import csv

def getAlbumFromId(album,database):
    conn = sqlite3.connect(database)
    cur = conn.cursor()
    cur.execute("SELECT * FROM albums WHERE AlbumId = '%s'" % album)
    rows = cur.fetchall()
    return rows[0]

def human_size(milliseconds):
    minutes = milliseconds/60000
    return minutes

def writeToFile(file,something):
    f = open(file, 'w')
    for thing in something:
        f.write(thing)
        f.write(",")
    f.close()

def getArtists(database):
    artists = []
    conn = sqlite3.connect(database)
    cur = conn.cursor()
    cur.execute("SELECT * FROM artists")
    rows = cur.fetchall()
    for row in rows:
        artists.append(row[1])
    return artists

def getTracksLongerThan10Minutes(database):
    tracks = []
    conn = sqlite3.connect(database)
    cur = conn.cursor()
    minutes = 600000
    cur.execute("SELECT * FROM tracks WHERE Milliseconds >= '%d'" % minutes)
    rows = cur.fetchall()
    for row in rows:
        if row[5]!= None:
            if ", " in row[5]:
                composers = row[5].split(", ")
            elif "/" in row[5]:
                composers = row[5].split("/")
            elif "&" in row[5]:
                composers = row[5].split(" & ")
            else:
                tracks.append((row[5],row[1],row[2],str(row[6])))
                continue
            for composer in composers:
                if composer not in tracks:
                    tracks.append((composer,row[1],row[2],str(row[6])))
    return tracks

try:
    if sys.argv[1] == "list-artists" and len(sys.argv)==2:
        print("+ {} +".format("-" * 30))
        for artist in getArtists("/home/ovi/chinook.db"):
            print("|{:^30}{:<90}".format(artist.encode("utf-8"),"|"))
            print("+ {} +".format("-"*30))
    elif sys.argv[1]=="long-tracks":
        if len(sys.argv)==3:
            tracks = getTracksLongerThan10Minutes('/home/ovi/chinook.db')
            print("+ {} +".format("-" * 120))
            print("|{:^30}| {:^20}  |   {:^10}  |   {:<5}|".format('artist', 'song', 'album', 'minutes',"|"))
            cnt = 0
            for track in tracks:
                cnt+=1
                page = int(sys.argv[2][5:])
                if cnt >= 12*(page-1):
                    minutes = str(human_size(int(track[3])))
                    album = getAlbumFromId(track[2],'/home/ovi/chinook.db')
                    print("|{:^30}| {:^20}  |   {:^10}  |   {:<5}|".format(track[0],track[1],album[1],minutes.encode("utf-8"), "|"))
                    print("+ {} +".format("-" * 120))
                if cnt == 12*page and cnt >=12*(page-1):
                    break
        elif len(sys.argv)==4:
            tracks = getTracksLongerThan10Minutes('/home/ovi/chinook.db')
            if sys.argv[2]=="--output":
                with open(sys.argv[3], 'w+') as csvfile:
                    spamwriter = csv.writer(csvfile, delimiter=',')
                    spamwriter.writerow(['artist', 'song', 'album', 'minutes'])
                    for track in tracks:
                        minutes = str(human_size(int(track[3])))
                        album = getAlbumFromId(track[2], '/home/ovi/chinook.db')
                        spamwriter.writerow([track[0],track[1],album[1],minutes])

    else:
        l = []
        l[1]+=1
except IndexError, ValueError:
    print("Arguments not valid")

