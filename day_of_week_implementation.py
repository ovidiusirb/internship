def day_of_week(m,d,y):
    year = y - (14 - m)/12
    x = year + year/4 - year/100 + year/400
    month = m + 12 *((14-m)/12) - 2
    day = (d + x + 31*month/12)%7

    return day

print(day_of_week(8,2,1953))