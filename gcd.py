def gcd(a,b):
    while b!= 0:
        t = b
        b= a % b
        a = t
    return a

def test_gcd():
    assert gcd(84,60)==12
    assert gcd(100,0)==100
    assert gcd(0,100)==100