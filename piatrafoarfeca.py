import random

def menu():
    print("1-Scissors\n")
    print("2-Paper\n")
    print("3-Rock\n")
    print("0-Exit\n")
    return input("Please enter command:\n")

def play(menu,score):
    choice = ["scissors","paper","rock"]
    r = random.randint(0,2)
    if menu == 0:
        return False
    elif choice[r] == choice[menu-1]:
        print "Both player and computer choose {}.\n It's a tie.\n".format(choice[r])
    elif choice[r] == "scissors" and choice[menu-1] == "paper":
        score[0]+=1
        print "Computer wins.\n Score is {}.\n".format(score)
    elif choice[r] == "paper" and choice[menu-1] == "scissors":
        score[1]+=1
        print "Player wins.\n Score is {}.\n".format(score)
    elif choice[r] == "rock" and choice[menu-1] == "paper":
        score[1]+=1
        print "Player wins.\n Score is {}.\n".format(score)
    elif choice[r] == "paper" and choice[menu-1] == "rock":
        score[0]+=1
        print "Computer wins.\n Score is {}.\n".format(score)
    elif choice[r] == "scissors" and choice[menu-1] == "paper":
        score[0]+=1
        print "Computer wins.\n Score is {}.\n".format(score)
    elif choice[r] == "paper" and choice[menu-1] == "scissors":
        score[1]+=1
        print "Player wins.\n Score is {}.\n".format(score)
    elif choice[r] == "scissors" and choice[menu-1] == "paper":
        score[1]+=1
        print "Player wins.\n Score is {}.\n".format(score)
    elif choice[r] == "rock" and choice[menu-1] == "scissors":
        score[0]+=1
        print "Computer wins.\n Score is {}.\n".format(score)



def main():
    score = [0, 0]
    while True:
        choice = menu()
        play(choice,score)

main()