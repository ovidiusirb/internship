
def un_flatten(list):
    dict = {}
    for i in list:
        dict_into_dict(i,0,dict)
    return dict

def dict_into_dict(i,j,dict):
    if j == len(i)-1:
        dict[i[j]]={}
        return
    if i[j]=='/':
        dict[i[j-1]]=i[j+1]
        dict[i[j+1]]={}
        j+=1
    dict = {}
    dict_into_dict(i,j+1,dict[i[j+1]])
'''
def un_flatten(list):
    d = {}
    for i in list:
        for j in range(0,len(i),1):
            if i[j]=="/":
                d[i[j-1]]={i[j+1]:{}}

            if j == len(i):
                d[i[j]]={}
    return d
'''
def test_un_flatten():
    tree = un_flatten(['A/B/T','A/U','A/U/Z'])
    assert tree == {
        'A':{
            'B':{'T':{}},
            'U':{
                'Z':{}
            }
        }
    }
    assert tree['A']['B']['T']=={}