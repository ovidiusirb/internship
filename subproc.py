import subprocess
import getpass
import tempfile

def main():
    usr = getpass.getuser()
    pth = tempfile.mkstemp()[1]
    pth += '_{}.mpg'.format(usr)

    proc = subprocess.Popen([
        'ffmpeg',
        '-f',
        'video4linux2',
        '-s',
        '640x480',
        '-i',
        '/dev/video0',
        '-ss',
        '0:0:2',
        '-frames',
        '120',
        pth
    ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    # this will wait for proc to terminate and returns stdout sterr
    proc.communicate()

    # this could have been an upload ...
    subprocess.Popen([
        'xdg-open',
        pth
    ])


if __name__ == '__main__':
    main()