from JsonFile import JsonFile
from ToDo import ToDo

class DB(object):
    def __init__(self,toDoList):
        self.toDoList=toDoList

    def setToDone(self,id):
        '''
        Searches for the toDo with the given id in the toDo list
        :param message:
        throws NameError if not found
        '''
        found = False
        for todo in self.toDoList:
            if todo.id == id:
                todo.done=True
                found = True
        if not found:
            raise NameError("toDo not in the list")

    def addToDo(self,message,created):
        '''
        :param message: toDo's message
        :param id: toDo's id
        :param created: toDo's create date
        :return: 
        '''
        try:
            id = self.get_next_id()
        except IndexError:
            id = 1

        t = ToDo(message, id, False,created)
        self.toDoList.append(t)


    def getNotDone(self):
        '''
        :return: a list containing the toDo's with done equal to False 
        '''
        rlist = []
        for todo in self.toDoList:
            if not todo.done:
                rlist.append(todo)
        return rlist

    def get_next_id(self):
        ids = []
        for t in self.toDoList:
            ids.append(t.id)
        return max(ids)+1