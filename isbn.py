# this function should return a tuple
# the first element is a list of integers representing the isbn
# the second element is the check digit
# parse_isbn('ISBN817525766-0') == ([8, 1, 7, 5, 2, 5, 7, 6, 6], 0)

def parse_isbn(isbn):
    isbn_check = int(isbn[-1])
    isbn_main = []
    for letter in isbn:
        if letter == '-':
            break
        try:
            digit = int(letter)
            isbn_main.append(digit)
        except ValueError:
            pass
    return isbn_main, isbn_check

def is_isbn_valid(isbn):
    checksum = 0
    isbn_main,isbn_check = parse_isbn(isbn)
    cnt = 10
    for digit in isbn_main:
        checksum+= digit*cnt
        cnt-=1
    checksum+= isbn_check*1
    return checksum % 11 == 0

# py.test code below

def test_parse_isbn():
    assert parse_isbn('ISBN817525766-0') == ([8, 1, 7, 5, 2, 5, 7, 6, 6], 0)

def test_is_isbn_valid():
    assert is_isbn_valid('ISBN817525766-0')
    assert not is_isbn_valid('ISBN817525767-0')
