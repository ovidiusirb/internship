#!/usr/bin/python
import os
import sys
from colored_term import colored_text

def get_extension(filename):
    string = ""
    r = range(len(filename)-1,0,-1)
    for i in r:
        if filename[i]=='.' and filename[i-1]=='/':
            return "hidden"
        if filename[i]=='.':
            r2 = range(i+1,len(filename),1)
            for i2 in r2:
                string+=filename[i2]
            break
    return string


def human_size(size):
    sufix = "bkMG"
    suf=""
    for s in sufix:
        if size < 1024:
            suf = s
            break
        size = size/1024
        suf = s

    return '{}{}'.format(size,suf)

#print human_size(1024),'\n'

def memory(size):
    string = ""
    b = 1024
    kb = 1024*1024
    mb = 1024*1024*1024
    gb = 1024*1024*1024*1024
    if size < b:
        string+=str(size)+ " b"
    elif size >= kb and size <mb:
        string+=str(size)+ " kb"
    elif size >= mb and size <gb:
        string+=str(size)+ " mb"
    elif size >= gb:
        string+=str(size)+ " gb"
    return string

def listdir_hidden(path):
    if os.path.isdir(path):
        for f in os.listdir(path):
            if f.startswith('.'):
                return True
    return False

def prettyls(filename):
    for e in sorted(os.listdir(filename)):
        e = os.path.join(filename,e)
        if os.path.isdir(e):
            files = 0
            for file in os.listdir(e):
                files+=1
            print "{:<40}{:^40}{:>40}".format(e, "", str(files)+" item(s)")
        elif os.path.isfile(e):
            size = os.path.getsize(e)
            if get_extension(e)=="hidden":
                print "{:<40}{:^40}{:>40}".format('\x1b[' + str(30)+'m'+e, "", human_size(size))+'\033[0m'
            elif get_extension(e)=="py":
                print "{:<40}{:^40}{:>40}".format('\x1b[' + str(32)+'m'+e, "py", human_size(size))+'\033[0m'
            else:
                print "{:<40}{:^40}{:>40}".format(e,get_extension(e), human_size(size))


prettyls(sys.argv[1])    #pentru argumentele din consola

