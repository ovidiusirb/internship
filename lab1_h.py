from __future__ import division
# Homework problems for unit 1
# ----------------------------
import math


#The rest of dividing a square number like 81, by 32 can only be
#0, 1, 4, 9, 16, 17, 25
#A number that has such a rest when divided by 32 is called a pseudo square mod 32
def pseudo_square_mod32(n):
    rest = [0,1,4,9,16,17,25]
    if n % 32 in rest:
        return True
    else:
        return False

def certain_square(n):
    return int(n**0.5 + 0.5)**2 == n

# Test how useful is the maybe_square test.
# How many false positives it reports for numbers under 1000?
# What percentage of tests are false positives?
def maybe_square_accuracy():
    N = 10000
    pass

# Given a list of numbers, return a list where
# all adjacent == elements have been reduced to a single element,
# so [1, 2, 2, 3] returns [1, 2, 3]. You may create a new list or
# modify the passed in list.
def remove_adjacent(nums):
    newList = []
    for i in nums:
        if i not in newList:
            newList.append(i)
    return newList

# Receives a color intensity as a int.
# 0 is black, 255 is the most saturated red, 128 is a middle red
# It should return the color in a float space where 0.0 is black
# and 1.0 is the most saturated red. 0.5 is that middle red.
# int_color_to_float_color(128) == 0.5
def int_color_to_float_color(red):
    color = float(red/255)*10
    color = int(color)
    color /= 10
    return color

# similar to the above case, but now we want to map black to tm
# and the reddest of reds, the 255 to tM
# int_color_to_float_range(128, 0, 2) == 1.0
def int_color_to_float_range(red, tm, tM):
    color = int_color_to_float_color(red)
    diff = tM - tm
    color *= diff
    return tm+color

# rescale transforms a number from the range fm..fM to the tm..tM range
# This is the generalization of the color use case
# but now f ranges not from 0..255 but from fm..fM
def rescale(f, fm, fM, tm, tM):
    f = float(f)
    diff1 = fM - fm
    diff2 = tM - tm
    div1 = diff1/(f-fm)
    f = tm + (diff2/div1)

    return f

def test_int_color_to_float_color():
    assert abs(int_color_to_float_color(51) - 0.2) < 1e-2
    assert abs(int_color_to_float_color(155) - 0.6078) < 1e-2

def test_int_color_to_float_range():
    assert abs(int_color_to_float_range(51, 1, 2) - 1.2) < 1e-2
    assert abs(int_color_to_float_range(128, -2, 2) - 0) < 1e-2


def test_pseudo_square_mod32():
    assert pseudo_square_mod32(9)
    assert not pseudo_square_mod32(24)
    assert pseudo_square_mod32(17)

def test_remove_adjacent():
    assert remove_adjacent([1, 2, 2, 3]) == [1, 2, 3]
    assert remove_adjacent([2, 2, 3, 3, 3]) == [2, 3]
    assert remove_adjacent([]) == []

def test_rescale():
    assert rescale(5, 0, 10, 0, 1) == 0.5
    assert abs(rescale(3, 2, 7, -1, 2) + 0.4) < 1e-4
