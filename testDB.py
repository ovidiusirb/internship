from DB import DB
from ToDo import ToDo

def setUp():
    t1 = ToDo("Sfadeste-i",1,False,'01.01.2017')
    t2 = ToDo("Sterge .idea",2,True,'02.02.2017')
    t3 = ToDo("Iesi la bere",3,False,'04.04.2017')
    db = DB([t1,t2])
    return t1,t2,t3,db

def testSetToDone():
    t1, t2, t3, db = setUp()
    db.setToDone(1)
    for todo in db.toDoList:
        if todo.id == 1 and not todo.done:
            assert False

def testaddToDo():
    t1, t2, t3, db = setUp()
    db.addToDo(t3.message,t3.created) #todo fara id
    assert len(db.toDoList)==3
    db.addToDo(t3.message, t3.created)
    assert len(db.toDoList) == 4

def testGetNotDone():
    t1, t2, t3, db = setUp()
    db = DB([t1,t2,t3])
    notDone = db.getNotDone()
    assert notDone == [t1,t3]
