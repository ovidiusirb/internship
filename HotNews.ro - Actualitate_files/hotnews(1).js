/**
 * @author: Tremend Software Consulting
 */
Prototype.Browser.IE6 = Prototype.Browser.IE && parseInt(navigator.userAgent.substring(navigator.userAgent.indexOf("MSIE")+5))==6;
Prototype.Browser.IE7 = Prototype.Browser.IE && !Prototype.Browser.IE6;

/**
 * Upload manager with methods for starting the upload (start monitoring and submitting the form) and checking the upload progress
 */
var uploadMgr = {
    /**
     * Uploads the item specified with the position
     * @param pos           the identifier of the upload slot
     * @param callbackName  what to do when upload is done
     */
    startUpload: function(pos, callbackName, acceptedTypes) {
        var uploadFile = $("uploadFile" + pos);
        var uploadForm = $("uploadForm" + pos);
        if (!(objIsValid(uploadFile)
            && objIsValid(uploadForm)
            )) {
            this.uploadFailed(pos);
            return;
        }
        var name = uploadFile.value;
        var reImg = /(.)*.(jpg|jpeg|gif|png)/i;
        var re = reImg;
        if(typeof(acceptedTypes) != "undefined" && acceptedTypes != null) {
            re = acceptedTypes;
            if(!name.match(re)) {
                alert("Fisierul nu este de tip imagine sau video");
                this.uploadFailed(pos);
                return;
            }
            if(name.match(reImg)) {
                uploadForm.action = "/upload";
            } else {
                uploadForm.action = "/uploadVideo";
            }
        } else {
            if(!name.match(re)) {
                alert("Fisierul nu este de tip imagine");
                this.uploadFailed(pos);
                return;
            }
        }
        uploadMgr.startUploadMonitoring(pos, callbackName);
        uploadForm.submit();
    },

    /**
     * Called when upload finishes
     * @param pos the identifier of the upload slot
     */
    uploadFinished: function(pos) {
    },

    /**
     * Called when upload fails
     * @param pos the identifier of the upload slot
     */
    uploadFailed: function(pos) {
        var uploadButton = $("uploadButton" + pos);
        if (objIsValid(uploadButton)) {
            disableAnchor(uploadButton, false);
        }
    },

    /**
     * Called periodically to check the status of the upload
     * @param pos           the identifier of the upload slot
     * @param callbackName  what to do when upload finishes
     */
    checkStatus: function(pos, callbackName) {
        siteUploadProxy.getStatus(function(stat) {
            if (stat.status == 1) {
                uploadMgr.updateProgressBar(pos, stat.percentComplete);
                window.setTimeout("uploadMgr.checkStatus(" + pos + (objIsValid(callbackName) ? ",'" + callbackName + "'" : "") + ")", 500);
            } else if (stat.status == 2) {
                uploadMgr.updateProgressBar(pos, 100);
                if(objIsValid(callbackName)) {
                    eval(callbackName + "()");
                }
                uploadMgr.uploadFinished(pos);
            } else if (stat.status == 3) {
                alert("Eroare: " + stat.message);
                uploadMgr.uploadFailed(pos);
                if(objIsValid(callbackName)) {
                    eval(callbackName + "()");
                }
            } else if (stat.status == 4) {
                window.setTimeout("uploadMgr.checkStatus(" + pos + (objIsValid(callbackName) ? ",'" + callbackName + "'" : "") + ")", 500);
            }
        });
    },

    /**
     * Updates the progress bar (percentage monitoring)
     * @param pos           the identifier of the upload slot
     * @param percentage    the status
     */
    updateProgressBar: function(pos, percentage) {
        $("uploadProgress" + pos).innerHTML = percentage + "%";
    },

    /**
     * Starts checking the status of the upload. The method is called after half second to give the upload form the chance to submit itself.
     * Otherwise the checking will mess status indicators on the server.
     * @param pos           the identifier of the upload slot
     * @param callbackName  what to do when upload finishes
     */
    startUploadMonitoring: function(pos, callbackName) {
        window.setTimeout("uploadMgr.checkStatus(" + pos + (objIsValid(callbackName) ? ",'" + callbackName + "'" : "") + ")", 500);
        return true;
    }
};

/**
 * Enables/disables an anchor
 * @param obj       the anchor obj
 * @param disable   true to disable, false to enable
 */
function disableAnchor(obj, disable){
    if(disable && !obj.disabled) {
        obj.disabled = true;
        var href = obj.getAttribute("href");
        if(href && href != "" && href != null) {
            obj.setAttribute('href_bak', href);
        }
        obj.removeAttribute('href');
        obj.style.color="gray";
    } else {
        obj.disabled = false;
        obj.setAttribute('href', obj.attributes['href_bak'].nodeValue);
        obj.style.color="blue";
    }
}

// Setup the EditInPlace area were we'll be keeping all our
// data and code.
var EditInPlace = Class.create();

// Default values for options, text, templates and states.
EditInPlace.defaults = {
	// Options that you'll commonly be overriding.
	id:						false,
	save_url:				false,
	form_type:				"text", // valid: text, textarea, select
	auto_adjust:			false,
	size:					false, // calculate at run time
	max_size:				60,
	rows:					false, // calculate at run time
	max_rows:				25,
	cols:					60,
	save_on_enter:			true,
	cancel_on_esc:			true,
	focus_edit:				true,
	select_text:			false, // problems in WebKit/Safari?
	click_event:			"click", // valid: click, dblclick
	more_data:				false,
	select_options:			false,
	external_control:		false,
	callback:		        null,

	// Text we display at various points
	edit_title:				"Clic aici pentru a schimba descrierea",
	empty_text:				"Clic aici pentru a schimba descrierea",
	saving_text:			"Se salveaza ...",
	savebutton_text:		"Salveaza",
	cancelbutton_text:		"Renunta",
	savefailed_text:		"Eroare la salvare.",

	// CSS classes and colors we use
	mouseover_highlight:	"#ffff99",
	editfield_class:		"eip_editfield",
	savebutton_class:		"eip_savebutton",
	cancelbutton_class:		"eip_cancelbutton",
	saving_class:			"eip_saving",
	empty_class:			"eip_empty",

    // Templates.  Most people won't need to change this, if you
	// do be careful, it can easily break things.
	saving:					'<span id="#{saving_id}" class="#{saving_class}" style="display: none;">#{saving_text}</span>',
	text_form:				'<input type="text" size="#{size}" maxlength="200" value="#{value}" id="#{id}_edit" name="#{id}_edit" class="#{editfield_class}" /> <br />',
	textarea_form:			'<textarea cols="#{cols}" rows="#{rows}" id="#{id}_edit" name="#{id}_edit" class="#{editfield_class}">#{value}</textarea> <br />',
	start_select_form:		'<select id="#{id}_edit" name="#{id}_edit" class="#{editfield_class}" /> <br />',
	select_option_form:		'<option id="#{id}_option_#{option}" name="#{id}_option_#{option}" value="#{option}" #{selected}>#{option_text}</option>',
	stop_select_form:		'</select>',
	start_form:				'<div class="contestTitle" id="#{id}_editor" style="display: none;">',
	stop_form:				'</div>',
	form_buttons:			'<span><input type="button" value="#{savebutton_text}" id="#{id}_save" name="#{id}_save" class="#{savebutton_class}" />&nbsp;&nbsp;<input type="button" value="#{cancelbutton_text}" id="#{id}_cancel" name="#{id}_cancel" class="#{cancelbutton_class}" /> </span>',

	// Private options that are managed for you,
	// don't touch these.
	is_empty:				false,
	orig_text:				false,
	orig_text_length:		false,
	orig_text_encoded:		false,
	orig_bk_color:			false
};

EditInPlace.prototype = {
	// Constructor
	initialize: function(options) {
		// Start with the defaults and over ride with
		// the specific options were provided.
		this.opt = {};
		Object.extend(this.opt, EditInPlace.defaults);
		Object.extend(this.opt, options || { });
	},

	// Public methods

	// Make an element editable
	edit: function() {
		var opt = this.opt;
		var id = opt['id'];

		// Set the title
		$(id).title = opt['edit_title'];

		// Save and process original content
		this._saveOrigText();

		// Turn on event processing
		this._watchForEvents();
	},

	// Private methods

	// Save and process the original contents
	_saveOrigText: function() {
		var opt = this.opt;
		var id = opt['id'];

		// Save the contents and note the length
		opt['orig_text'] = $(id).innerHTML;
		opt['orig_text_length'] = opt['orig_text'].length;

		// Find the original background color
		opt['orig_bk_color'] = $(id).getStyle('background-color');
		var bk_id = id;
		while(!opt['orig_bk_color']) {
			try {
				bk_id = $(bk_id).up();
			}
			catch(err) {
				break;
			}
		}

		// If no color was found default to all white
		if(!opt['orig_bk_color']) {
			opt['orig_bk_color'] = "#ffffff";
		}

		// Ugly hack for WebKit/Safari
//		if(Prototype.Browser.WebKit) {
//			opt['orig_bk_color'] = '#ffffff';
//		}

		// For select edits find the original option value
		if(opt['form_type'] == 'select') {
			for(var i in opt['select_options']) {
				if(opt['select_options'][i] == opt['orig_text']) {
					opt['orig_option'] = i;
					break;
				}
			}
		}

		// If auto_adjust is turned on determine the edit method to use
		if(opt['auto_adjust']) {
			if(opt['orig_text_lenth'] > opt['max_size']) {
				opt['form_type'] = 'textarea';
			}
			else {
				opt['form_type'] = 'text';
			}
		}

		// If the element was previously marked as empty but isn't
		// empty anymore then update the empty state and remove
		// the empty class
		if(opt['is_empty']) {
			if(!$(id).empty()) {
				opt['is_empty'] = false;
				$(id).removeClassName(opt['empty_class']);
			}
		}

		// If the element is currently empty update the empty state and class
		if($(id).empty()) {
			opt['is_empty'] = true;
			$(id).innerHTML = opt['empty_text'];
			$(id).addClassName(opt['empty_class']);
		}

		// Encode < > "
		opt['orig_text_encoded'] = opt['orig_text'].replace(/</g, '&lt;');
		opt['orig_text_encoded'] = opt['orig_text'].replace(/>/g, '&gt;');
		opt['orig_text_encoded'] = opt['orig_text'].replace(/"/g, '&quot;');
	},

	// Turn on event listening
	_watchForEvents: function() {
		var opt = this.opt;
		var id = opt['id'];

		// Bind event listeners
		opt['mouseover']	= this._mouseOver.bindAsEventListener(this, id);
		opt['mouseout']		= this._mouseOut.bindAsEventListener(this, id);
		opt['mouseclick']	= this._mouseClick.bindAsEventListener(this, id);
		opt['canceledit']	= this._cancelEdit.bindAsEventListener(this, id);
		opt['saveedit']		= this._saveEdit.bindAsEventListener(this, id);

		// Watch for events
		$(id).observe('mouseover', opt['mouseover']);
		$(id).observe('mouseout', opt['mouseout']);
		$(id).observe(opt['click_event'], opt['mouseclick']);

		// External control events
		if(opt['external_control']) {
			var ext_id = opt['external_control'];
			$(ext_id).observe('mouseover', opt['mouseover']);
			$(ext_id).observe('mouseout', opt['mouseout']);
			$(ext_id).observe(opt['click_event'], opt['mouseclick']);
		}
	},

	// Mouse over event handling
	_mouseOver: function(e) {
		var opt = this.opt;
		var id = opt['id'];

		$(id).setStyle({backgroundColor: opt['mouseover_highlight']});
	},

	// Mouse out event handling
	_mouseOut: function(e) {
		var opt = this.opt;
		var id = opt['id'];

		$(id).setStyle({backgroundColor: opt['orig_bk_color']});
	},

	// Mouse click event handling, go into edit mode
	_mouseClick: function(e) {
        if(dndIng) {
            dndIng = false;
            return true;
        }
        var opt = this.opt;
		var id = opt['id'];

		// Hide the original element
		$(id).hide();

		// If there is an external control hide it too
		if(opt['external_control']) {
			$(opt['external_control']).hide();
		}

		// Compile the start of the edit form
		var form			= '';
		var start_form		= new Template(opt['start_form']);
		var stop_form		= new Template(opt['stop_form']);
		var form_buttons	= new Template(opt['form_buttons']);
		form += start_form.evaluate({id: id});

		// Put together the body of the form
		switch(opt['form_type']) {
			case 'text':
				var size = opt['orig_text_length'] + 15;
				if(size > opt['max_size']) {
					size = opt['max_size'];
				}
				size = (opt['size'] ? opt['size'] : size);

				var text_form = new Template(opt['text_form']);
				form += text_form.evaluate({
					id: id,
					size: size,
					value: opt['orig_text_encoded'],
					editfield_class: opt['editfield_class']
				});

				break;
			case 'textarea':
				var rows = (opt['orig_text_length'] / opt['cols']) + 2;
				for(var i = 0; i < opt['orig_text_length']; i++) {
					if(opt['orig_text'].charAt(i) == "\n") {
						rows++;
					}
				}
				if(rows > opt['max_rows']) {
					rows = opt['max_rows'];
				}
				rows = (opt['rows'] ? opt['rows'] : rows);

				var textarea_form = new Template(opt['textarea_form']);
				form += textarea_form.evaluate({
					id: id,
					cols: opt['cols'],
					rows: rows,
					value: opt['orig_text_encoded'],
					editfield_class: opt['editfield_class']
				});

				break;
			case 'select':
				var start_select_form = new Template(opt['start_select_form']);
				form += start_select_form.evaluate({
					id: id,
					editfield_class: opt['editfield_class']
				});

				var option_form = new Template(opt['select_option_form']);
				var selected = '';
				for(var i in opt['select_options']) {
					if(opt['select_options'][i] == opt['orig_text']) {
						selected = 'selected="selected"';
					}
					else {
						selected = '';
					}

					form += option_form.evaluate({
						id: id,
						option: i,
						selected: selected,
						option_text: opt['select_options'][i]
					});
				}

				var stop_select_form = new Template(opt['stop_select_form']);
				form += stop_select_form.evaluate({});

				break;
		}

		// Compile the end of the edit form
		form += form_buttons.evaluate({
			id: id,
			savebutton_class: opt['savebutton_class'],
			savebutton_text: opt['savebutton_text'],
			cancelbutton_class: opt['cancelbutton_class'],
			cancelbutton_text: opt['cancelbutton_text']
		});
		form += stop_form.evaluate({});

		this._displayForm(form);
	},

	// Save edit
	_saveEdit: function() {
		var opt = this.opt;
		var id = opt['id'];

		// Gather up all of the data to pass in the XHR.
		var params = {
			'id': id,
			'form_type': opt['form_type'],
			'old_content': opt['orig_text'],
			'new_content': $F(id + '_edit')
		};

		// Provide details on the options if this was a select edit
		if(opt['form_type'] == 'select') {
			params['new_option'] = params['new_content'];
			params['new_option_text'] = $(id + '_option_' + params['new_content']).innerHTML;

			params['old_option'] = opt['orig_option'];
			params['old_option_text'] = opt['orig_text'];

			// Over ride the *_content to use the *_option_text instead
			params['old_content'] = params['old_option_text'];
			params['new_content'] = params['new_option_text'];
		}

		// Glue all of the parameters together and escape content
		var post_data = '';
		for(var i in params) {
			post_data += '&' + i + '=' + encodeURIComponent(params[i]);
		}

		// Include any additional data that was provided
		if(opt['more_data']) {
			for(var i in opt['more_data']) {
				post_data += '&' + i + '=' + encodeURIComponent(params[i]);
			}
		}

		// Strip the first & off of the front of post_data
		post_data.sub('&', '', 1);

		// Put the saving message together
		var saving = new Template(opt['saving']);
		saving = saving.evaluate({
			saving_id: id + '_saving',
			saving_class: opt['saving_class'],
			saving_text: opt['saving_text']
		});

		// Remove the edit form
		$(id + '_editor').remove();

		// Show the saving message
		$(id).insert({after: saving});
		$(id + '_saving').show();

		// Need a copy of this object to deal with content issues
		var my_obj = this;

        //alert(this.opt['callback']);
        this.opt['callback'](my_obj, id, params['new_content']);
    },

	// Cancel edit
	_cancelEdit: function() {
		var opt = this.opt;
		var id = opt['id'];

		$(id + '_editor').remove();
		$(id).show();

		// Make sure the mouse over highlight is turned off
		$(id).setStyle({backgroundColor: opt['orig_bk_color']});

		if(opt['external_control']) {
			$(opt['external_control']).show();
		}
	},

	// Display edit form
	_displayForm: function(form) {
		var opt = this.opt;
		var id = opt['id'];

		// Add the form after the original element
		$(id).insert({after: form});
		$(id + '_editor').show();

		// Focus on edit
		//if(opt['focus_edit']) {
			$(id + '_edit').focus();
		//}

		// Select edit text
		//if(opt['select_text']) {
			$(id + '_edit').select();
		//}

		// Watch for save and cancel button click
		$(id + '_save').observe('click', opt['saveedit']);
		$(id + '_cancel').observe('click', opt['canceledit']);

		// Need a copy of this to deal with context issues
		var my_obj = this;

		// Watch for the enter key
		if(opt['save_on_enter']) {
			$(id + '_edit').observe(
				'keypress',
				function(e) {
					if(e.keyCode == Event.KEY_RETURN) {
						my_obj._saveEdit();
					}
				}
			);
		}

		// Watch for the escape key
		if(opt['cancel_on_esc']) {
			$(id + '_edit').observe(
				'keypress',
				function(e) {
					if(e.keyCode == Event.KEY_ESC) {
						my_obj._cancelEdit();
					}
				}
			);
		}
	}
};

// Attach EditInPlace to $()
//Element.addMethods({
	editInPlace =  function(element, options, callback) {
        if(!options) {
			var options = {};
		}

		options['id'] = $(element).id;
		options['callback'] = callback;

		// Tack on additional parameters to the options data
		Object.extend(options, arguments[1]);

		// Create a new object
		var eip = new EditInPlace(options);
		eip.edit();
	}
//});


// Font size increase/decrease
var minFontSize = 8;
var maxFontSize = 22;
var MIN_FONT_SIZE = 8;
var MAX_FONT_SIZE = 32;
var FONT_SIZE_STEP = 2;

// Ticker
var memorywidth = "300px";
//scroller width
var memoryheight = "18px";
//scroller height
var memorybgcolor = "transparent";
//scroller background
var memorypadding = "2px";
//padding applied to the scroller. 0 for non.
var borderCSS = "color: white;";
//Border CSS, applied to scroller to give border.
var memoryspeed = 10;
//Scroller speed (larger is faster 1-10)
var pauseit = 1;
//Pause scroller onMousever (0=no. 1=yes)?
var persistlastviewedmsg = 1;
//should scroller's position persist after users navigate away (1=yes, 0=no)?
var persistmsgbehavior = "onload";
//set to "onload" or "onclick".
var memorycontent = "";
var combinedcssTable = "width:" + (parseInt(memorywidth) + 6) + "px;background-color:" + memorybgcolor + ";padding:" + memorypadding + ";" + borderCSS + ";";
var combinedcss = "width:" + memorywidth + ";height:" + memoryheight + ";";
var divonclick = (persistlastviewedmsg && persistmsgbehavior == "onclick") ? 'onClick="savelastmsg()" ' : '';
memoryspeed = (document.all) ? memoryspeed : Math.max(1, memoryspeed - 1);
//slow speed down by 1 for NS
var copyspeed = memoryspeed;
var pausespeed = (pauseit == 0) ? copyspeed : 0;
var iedom = document.all || document.getElementById;
var actualwidth = '';
var memoryscroller;
var tickerDelta = 10;

/**
 * Mouse over tabs
 */
function onhover(site, id) {
    $(id).src = site + 'images/tabs/' + id.split("_")[0] + "/" + id + '_hover.gif';
};

/**
 * Mouse out tabs
 */
function onout(site, id) {
    $(id).src = site + 'images/tabs/' + id.split("_")[0] + "/" + id + '.gif';
};

/**
 * DOM util function for inserting node before ref
 */
function insertBefore(/*Node*/node, /*Node*/ref) {
    var parent = ref.parentNode;
    parent.insertBefore(node, ref);
    return true;
};

/**
 * DOM util function for inserting node after ref
 */
function insertAfter(/*Node*/node, /*Node*/ref) {
    var parent = ref.parentNode;
    if (ref == parent.lastChild) {
        parent.appendChild(node);
    } else {
        return insertBefore(node, ref.nextSibling);
    }
    return true;
};

/**
 * Sets a cookie
 */
function setCookie(/*String*/name, /*String*/value, /*Number?*/days, /*String?*/path, /*String?*/domain, /*boolean?*/secure) {
    var expires = -1;
    if ((typeof days == "number") && (days >= 0)) {
        var d = new Date();
        d.setTime(d.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = d.toGMTString();
    }
    //alert("expires " + expires);
    value = escape(value);
    var cookies = name + "=" + value + ";"
        + (expires != -1 ? " expires=" + expires + ";" : "")
        + (path ? "path=" + path : "")
        + (domain ? "; domain=" + domain : "")
        + (secure ? "; secure" : "");
    //alert("cookies: " + cookies);
    document.cookie = cookies;
    //alert("document.cookie: " + document.cookie);
};

/**
 * Retrieves the value for a cookie.
 * This fixes an issue with the old method that used
 * document.cookie.indexOf( name + "=" );
 */
var getCookie = function(/*String*/cookieName, /*String*/defaultValue) {
    // first we'll split this cookie up into name/value pairs
    // note: document.cookie only returns name=value, not the other components
    var a_all_cookies = document.cookie.split(';');
    var a_temp_cookie = '';
    var cookie_name = '';
    var cookie_value = '';
    var b_cookie_found = false; // set boolean t/f default f

    for (var i = 0; i < a_all_cookies.length; i++) {
        // now we'll split apart each name=value pair
        a_temp_cookie = a_all_cookies[i].split('=');

        // and trim left/right whitespace while we're at it
        cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

        // if the extracted name matches passed cookieName
        if (cookie_name == cookieName) {
            b_cookie_found = true;
            // we need to handle case where cookie has no value but exists (no = sign, that is):
            if (a_temp_cookie.length > 1) {
                cookie_value = unescape(a_temp_cookie[1].replace(/^\s+|\s+$/g, ''));
            }
            // note that in cases where cookie is initialized but no value, null is returned
            return cookie_value;
        }
        a_temp_cookie = null;
        cookie_name = '';
    }
    if (!b_cookie_found) {
        return defaultValue;
    }
}

/*
 * Delete a cookie
 */
var deleteCookie = function (name, path, domain) {
    document.cookie = name + "="
        + (path ? ";path=" + path : "")
        + (domain ? ";domain=" + domain : "" )
        + ";expires=Thu, 01-Jan-1970 00:00:01 GMT";
}

function populatescroller() {
    memoryscroller = $("memoryscroller");
    memoryscroller.style.left = parseInt(memorywidth) + tickerDelta + "px";
    if (persistlastviewedmsg && getCookie("lastscrollerpos") != "") {
        revivelastmsg();
    }
    memoryscroller.innerHTML = memorycontent;
    actualwidth = $("temp").offsetWidth;

    window.setTimeout("scrollmarquee()", 500);
};

function savelastmsg() {
    document.cookie = "lastscrollerpos=" + memoryscroller.style.left;
};

function revivelastmsg() {
    lastscrollerpos = parseInt(getCookie("lastscrollerpos"));
    memoryscroller.style.left = parseInt(lastscrollerpos) + "px";
};

function scrollmarquee() {
    if (parseInt(memoryscroller.style.left) > (actualwidth * (-1) + tickerDelta)) {
        memoryscroller.style.left = parseInt(memoryscroller.style.left) - copyspeed + "px";
    } else {
        memoryscroller.style.left = (parseInt(memorywidth) - 100) + tickerDelta + "px";
    }
    window.setTimeout("scrollmarquee()", 500);
};

/**
 * Starts the ticker
 */
function runTheTicker() {
    // Go for the next story data block
    if (theCurrentLength == 0) {
        theCurrentStory++;
        theCurrentStory = theCurrentStory % theSummaries.length;
        theStorySummary = theSummaries[theCurrentStory].replace(/&quot;/g, '"');
    }

    // Stuff the current ticker text into the anchor
    $("tickerPlaceHolder").innerHTML = theStorySummary.substring(0, theCurrentLength);

    // Modify the length for the substring and define the timer
    var myTimeout;
    if (theCurrentLength != theStorySummary.length) {
        theCurrentLength++;
        myTimeout = theCharacterTimeout;
    } else {
        theCurrentLength = 0;
        myTimeout = theStoryTimeout;
    }
    // Call up the next cycle of the ticker
    setTimeout("runTheTicker()", myTimeout);
};

/**
 * Add functionality to page onload event
 */
function addOnLoad(fn) {
    if (window.addEventListener) {
        window.addEventListener("load", fn, false);
    } else if (window.attachEvent) {
        window.attachEvent("onload", fn);
    } else if (document.all || document.getElementById) {
        var oldHandler = window.onload;
        window.onload = function() {
            oldHandler();
            fn();
        }
    }
};

/**
 * Inits ticker
 */
function initTicker(content) {
    iedom = document.all || document.getElementById;
    memorycontent = content;
    if (iedom) {
        document.write('<span id="temp" style="visibility:hidden;position:absolute;top:-100px;left:-10000px">' + memorycontent + '</span>');
    }
    addOnLoad(populatescroller);

    if (persistlastviewedmsg && persistmsgbehavior == "onload") {
        window.onunload = savelastmsg;
    }

    if (iedom) {
        with (document) {
            document.write('<table border="0" cellspacing="0" cellpadding="0" style="' + combinedcssTable + '"><td>');
            write('<div style="position:relative;overflow:hidden;' + combinedcss + '" onMouseover="copyspeed=pausespeed" onMouseout="copyspeed=memoryspeed">');
            write('<div id="memoryscroller" style="position:absolute;left:0px;top:0px;" ' + divonclick + '></div>');
            write('</div>');
            document.write('</td></table>');
        }
    }
};

function getUrlParameter(paramName) {
    if (paramName.search(/[^\w\[\]]/) >= 0) {
        return null;
    }
    var url = document.location.href;
    var pos = url.indexOf('?');
    if (pos < 0) {
        return null;
    }
    url = url.substring(pos);
    paramName = paramName.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");
    var regex = new RegExp("[\\?&]" + paramName + "=([^&]*)");
    var result = url.match(regex);
    return (result != null) ? result[1] : null;
};

function typeInInputField(field) {
    field.typedIn = true;
}

function showInputFieldTip(field) {
    if (!field.typedIn) {
        field.value = field.value_bak;
    }
}

function hideInputFieldTip(field) {
    if (!field.typedIn) {
        field.value_bak = field.value;
        field.value = '';
    }
}

function typeInPasswordField(field) {
    field.typedIn = true;
}

function showPasswordFieldTip(field, id, loginFunctionName) {
    if (field.type != 'text') {
        var clone = document.createElement('input');
        clone.type = 'text';
        clone.id = id;
        clone.name = "j_password";
        clone.className = "login_password";
        clone.value = field.value_bak;
        field.parentNode.replaceChild(clone, field);
        window.setTimeout("$('" + id + "').focus();"
                + "Event.observe($('" + id + "'), 'focus', "
                + "function(e){hidePasswordFieldTip(this, '" + id + "'"
                + (objIsValid(loginFunctionName) ? ",'" + loginFunctionName + "'" : "")
                + ")});",
            10);
    }
}

function hidePasswordFieldTip(field, id, loginFunctionName) {
    if (field.type != 'password') {
        var clone = document.createElement('input');
        clone.type = 'password';
        clone.id = id;
        clone.name = "j_password";
        clone.className = "login_password";
        clone.value_bak = field.value;
        field.parentNode.replaceChild(clone, field);
        clone.focus();
/*
        window.setTimeout("$('" + id + "').focus();"
                + "Event.observe($('" + id + "'), 'blur', "
                + "function(e){showPasswordFieldTip(this, '" + id + "'"
                + (objIsValid(loginFunctionName) ? ",'" + loginFunctionName + "'" : "")
                + ")});",
                10);
*/
        if (objIsValid(loginFunctionName)) {
            window.setTimeout("$('" + id + "').focus();"
                    + "Event.observe($('" + id + "'), 'keypress', "
                    + "function(e){if(e.keyCode==Event.KEY_RETURN){" + loginFunctionName + "();}});",
                10);
        }
    }
}

/**
 * Checks for form submit by ENTER key
 */
function checkLoginEnter(e) {
    if (e.keyCode == Event.KEY_RETURN) {
		doLogin();
	}
};

/**
 * Checks for form submit by ENTER key
 */
function checkLoginEnter2(e) {
    alert('1');
    if (e.keyCode == Event.KEY_RETURN) {
		doLogin2();
	}
};

/**
 * Checks for form submit by ENTER key
 */
function checkLoginEnterSubmit(e) {
    if (e.keyCode == Event.KEY_RETURN) {
		doLoginSubmit();
	}
};

/**
 * Returns true if login is invalid
 */
function invalidLogin() {
    var url = document.location.href;
    var pos = url.indexOf("login_invalid");
    return (pos >= 0);
};

/**
 * Toggles the login container
 */
function toggleLogin() {
    if($('pozillaContainer')) {
        $('pozillaContainer').toggle();
    }
    $('loginPanel').toggle();
};

/**
 * Handles the login
 */
function doLoginSubmit() {
    $('loginForm').submit();
};

/**
 * Handles the login
 */
function doLogin() {
//    $('loginForm').submit();
    ajaxLogin('loginForm', 'loginMessage', '<img src="/images/new/ajax_loading_anim_2.gif" height="16" alt=""/> Se conecteaza...',
        'loginMessage', 'master_sus_login', 'loginPanel', doLogin1Success);
};

/**
 * Handles the login
 */
function doLogin2() {
//    $('master_sus_acegi_security_remember_me').value = $('master_sus_subpanel_acegi_security_remember_me').checked ? "on" : "";
    $('master_sus_subpanel_login').hide();
    ajaxLogin('loginForm2', 'master_sus_subpanel_message', '<img src="/images/new/ajax_loading_anim_1.gif" height="16" alt=""/> Se conecteaza...',
        'master_sus_subpanel_message', 'master_sus_login', 'loginPanel', doLogin2Success);
};

/**
 * Handles the login for hotreporter
 */
function checkKeyHR(e) {
    if (e.keyCode == Event.KEY_RETURN) {
		doLoginHR();
	}
}

/**
 * Handles the login for hotreporter
 */
function doLoginHR() {
    ajaxLogin(
        'loginFormHR',
        'hotreporter_login_message',
        '<img src="/images/new/ajax_loading_anim_1.gif" height="16" alt=""/> Se conecteaza...',
        'hotreporter_login_message',
        'master_sus_login',
        'loginPanel',
        doLoginHRSuccess
    );
};

/**
 * Performs the login via AJAX
 */
function ajaxLogin(loginForm, waitingMessageContainer, waitingMessage,
                   loginMessageContainer, replaceLoginContainer,
                   loginPanel, loginSuccessFunction) {
    loginForm = $(loginForm);
    var acegiRememberMe = $('master_sus_subpanel_acegi_security_remember_me');
    if (objIsValid(acegiRememberMe)) {
        acegiRememberMe.hide();
        insertBefore(acegiRememberMe, loginForm.firstChild);
    }
    Element.update(waitingMessageContainer, waitingMessage);
    Element.show(waitingMessageContainer);

    new Ajax.Request('/web_login_check', {
        method: 'post',
        postBody: Form.serialize(loginForm) + '&ajax=true',
        onSuccess: loginSuccessFunction
//        onSuccess: doLoginHRSuccess
    });
}

function ajaxLogin2() {
	new Ajax.Request('/web_login_check', {
		method: 'post',
		postBody: Form.serialize('loginAjaxForm') + '&ajax=true',
		onSuccess: doLoginHRSuccess,
		onFailure: function(response) {
            alert(response.obj);
        }
    });
}
function fbAjaxLogin(response) {
	new Ajax.Request('/facebook_login_check', {
		method: 'post',
		postBody: 'access_token=' + response.authResponse.accessToken + '&ajax=true',
		onSuccess: function(response) {
//            doLoginHRSuccess(response);
            setCookie("hn_check_fb", "true", 365, "/", ".hotnews.ro");
        }

    });
}
function twitterAjaxLogin(currentUser) {
	new Ajax.Request('/twitter_login_check', {
		method: 'post',
		postBody: 'name=' + currentUser.name + '&username=' + currentUser.screenName + '&ajax=true',
		onSuccess: function(response) {
//            doLoginHRSuccess(response);
            setCookie("hn_check_tw", "true", 365, "/", ".hotnews.ro");
        }
	});
}

function twitterLogin() {
	var twitterId = $('j_username3');
	if (objIsValid(twitterId)) {
		var twitterIdValue = twitterId.value.strip();
		if (twitterIdValue.length > 0){
			new Ajax.Request('/twitter_login_check', {
				method: 'post',
//				postBody: Form.serialize('loginTwitterAjaxForm') + '&ajax=true',
				postBody: 'j_username=' + twitterIdValue + '&j_password=' + $('j_password3').value + '&ajax=true',
				onSuccess: doLoginHRSuccess
			});
		}else{
			alert("Trebuie completat userul si parola");
		}
	}else{
		alert("Trebuie completat userul si parola");
	}
}

function facebookLogin(uid, username){
    new Ajax.Request('/facebook_login_check?' + 'ajax=true&user=' + uid + '&expires=0&&session_key=' + username, {
        method: 'get',
        onSuccess: doLoginHRSuccess
    });
}

function doLogin1Success(response) {
    doLoginSuccess(response, 'loginMessage','loginMessage', 'master_sus_login', 'loginPanel');
}

function doLogin2Success(response) {
    doLoginSuccess(response, 'master_sus_subpanel_message','master_sus_subpanel_message', 'master_sus_login', 'loginPanel');
}

function doLoginSuccess(response, waitingMessageContainer,
                        loginMessageContainer, replaceLoginContainer, loginPanel) {
    Element.hide(waitingMessageContainer);
    var msg = response.responseText;
    if (msg.startsWith("error:")) {
        Element.update(loginMessageContainer, '<font color="red" align="center">Login invalid</font>');
        Element.show(loginMessageContainer);
    } else if (msg.startsWith("url:")) {
        location.href = msg.substring("url:".length);
    } else if (msg.startsWith("success:")) {
        var nickname = msg.substring("success:".length);
        welcomeUsername  = nickname;
        if ($(replaceLoginContainer)) {
            Element.update(replaceLoginContainer, '\
                <div class="logged_in">\
                    <div class="salut" style="margin:3px 0 2px 0;">Salut ' + nickname.escapeHTML() + '!</div>\
                    <ul class="personal_links">\
                        <li><a href="/myhotnews/detalii_personale">Detalii personale</a></li>\
                        <li><a href="/myhotnews/comentariile_mele">Comentariile mele</a></li>\
                        <li><a href="/myhotnews/concursurile_mele">Concursurile mele</a></li>\
                    </ul>\
                </div>\
                <div class="underPanels" style="margin-top:7px; padding-left:7px; text-align:right; padding-right:7px;">\
                    <a class="logout_link" style="color:#454545;" href="javascript:doLogoutNew(\'/logout?a=\'+(new Date()).getTime())">Log-out</a>\
                </div>\
            ');
        }
        if ($(loginPanel)) {
            Element.hide(loginPanel);
        }
//        window.location.href = '/myhotnews';
    }
}

var stopPollingUser = false;
var pollCount = 0;

function onLoginClick() {
    stopPollingUser = false;
    pollCount = 0;
    pollUser();
}

function pollUser() {
    if(stopPollingUser || welcomeUsername != undefined || pollCount > 100) {
        return;
    }
    checkUserLoggedIn();
    pollCount++;
}

function checkUserLoggedIn() {
    var replaceLoginContainer = 'master_sus_login';
    var loginPanel = 'loginPanel';
    userProxy.checkUserLoggedIn(function(msg) {
        if (msg == "retry") {
            setTimeout("pollUser()", 500);
        } else if (msg.startsWith("success:")) {
            var nickname = msg.substring("success:".length);
            welcomeUsername = nickname;
            if ($(replaceLoginContainer)) {
                var d=new Date();
                Element.update(replaceLoginContainer,
                    /* '<div style="display: none"><iframe src="http://sso.hotnews.ro?service=http%3A%2F%2Fwww.webpr.ro%2Fj_spring_cas_security_check%3F_spring_security_remember_me%3Don"></iframe></div>\
                     <div style="display: none"><iframe src="http://sso.hotnews.ro?service=http%3A%2F%2Fwww.campusnews.ro%2Fj_spring_cas_security_check%3F_spring_security_remember_me%3Don"></iframe></div>\*/
                        '<div style="display: none"><iframe src="http://www.webpr.ro/secure/login_iframe"></iframe></div>\
                        <div style="display: none"><iframe src="http://www.campusnews.ro/secure/login_iframe"></iframe></div>\
                        <div class="logged_in">\
                            <div class="salut" style="margin:3px 0 2px 0;">Salut ' + nickname.escapeHTML() + '!</div>\
                            <ul class="personal_links">\
                                <li><a href="/myhotnews/detalii_personale">Detalii personale</a></li>\
                                <li><a href="/myhotnews/comentariile_mele">Comentariile mele</a></li>\
                                <li><a href="/myhotnews/concursurile_mele">Concursurile mele</a></li>\
                            </ul>\
                        </div>\
                        <div class="underPanels" style="margin-top:7px; padding-left:7px; text-align:right; padding-right:7px;">\
                            <a class="logout_link" style="color:#454545;" href="javascript:doLogoutNew(\'/logout?a=\'+(new Date()).getTime())">Log-out</a>\
                        </div>\
                    ');
            }
            if ($(loginPanel)) {
                Element.hide(loginPanel);
            }
            if ($jh("#colorbox").css("display")=="block") {
                closeLogin();
            }
        }
    });

}

function doLoginHRSuccess(response) {
    //var waitingMessageContainer = 'hotreporter_login_message';
    var loginMessageContainer = 'hotreporter_login_message';
    var replaceLoginContainer = 'master_sus_login';
    var loginPanel = 'loginPanel';
    //Element.hide(waitingMessageContainer);
    var msg = response.responseText;
    if (msg.startsWith("error:")) {
//        Element.update(loginMessageContainer, '<font color="red" align="center">Login invalid</font>');
//        Element.show(loginMessageContainer);
        alert("Login invalid");
    } else if (msg.startsWith("url:")) {
        location.href = msg.substring("url:".length);
    } else if (msg.startsWith("success:")) {
        var nickname = msg.substring("success:".length);
        welcomeUsername = nickname;
        if ($(replaceLoginContainer)) {
            Element.update(replaceLoginContainer, '\
                <div class="logged_in">\
                    <div class="salut" style="margin:3px 0 2px 0;">Salut ' + nickname.escapeHTML() + '!</div>\
                    <ul class="personal_links">\
                        <li><a href="/myhotnews/detalii_personale">Detalii personale</a></li>\
                        <li><a href="/myhotnews/comentariile_mele">Comentariile mele</a></li>\
                        <li><a href="/myhotnews/concursurile_mele">Concursurile mele</a></li>\
                    </ul>\
                </div>\
                <div class="underPanels" style="margin-top:7px; padding-left:7px; text-align:right; padding-right:7px;">\
                    <a class="logout_link" style="color:#454545;" href="javascript:doLogoutNew(\'/logout?a=\'+(new Date()).getTime())">Log-out</a>\
                </div>\
            ');
        }
        if ($(loginPanel)) {
            Element.hide(loginPanel);
        }
        if ($jh("#colorbox").css("display")=="block") {
            closeLogin();
        }
        if(window.location.href == "http://www.hotnews.ro/login")
            window.location.href='/myhotnews';

//        $('loginFormHR').hide();
//        with($('hotreport_nickname')) {
//            show();
//            disabled = true;
//            value = nickname;
//        }
//        with($('hotreport_email2')) {
//            show();
//            disabled = true;
//            value = $('hotreport_email').value;
//        }

//        window.location.href = '/myhotnews';
    }

}

function restoreRememberMeCheckbox() {
    var acegiRememberMe = $('master_sus_subpanel_acegi_security_remember_me');
    if (objIsValid(acegiRememberMe)) {
        $('master_sus_subpanel_message').hide();
        $('master_sus_subpanel_acegi_security_remember_me_container').appendChild(acegiRememberMe);
        acegiRememberMe.show();
        $('master_sus_subpanel_login').show();
    }
}

/**
 * Logs out the current logged in user
 */
/*
function doLogout(logoutUrl) {
    var opt = {
        method: 'post',
        postBody: 'ajax=true',
        onSuccess: function(response) {
            if ($('master_sus_login') != null) {
                welcomeUsername = null;
                Element.update('master_sus_login', '\
        <form action="/web_login_check" method="post" id="loginForm2" style="width:150px; overflow:hidden;">\
        <div class="tabs">\
            <div class="tab selected" id="master_sus_tab_signup" onclick="selectTabSignup()">\
                Cont nou\
            </div>\
            <div class="tab" id="master_sus_tab_login" onclick="selectTabLogin()">\
                Log-in\
            </div>\
        </div>\
        <div class="panels">\
            <div class="panel" id="master_sus_panel_signup">\
                <input type="text" name="signup_email" id="signup_email" value="Adresa e-mail" class="login_email"\
                       onkeydown="typeInInputField(this)" onclick="hideInputFieldTip(this)" onblur="showInputFieldTip(this)" />\
                <input type="button" name="master_sus_signup_button" id="master_sus_signup_button" value="Mă abonez la Newsletter" class="signup_button" onclick="subscribeToNewsletter()"/>\
                <br style="clear:both" />\
            </div>\
            <div id="master_sus_panel_login" style="display:none;">\
                    <input type="text" id="j_username2" name="j_username" value="Email" class="login_email"\
                       onkeydown="typeInInputField(this)" onclick="hideInputFieldTip(this);restoreRememberMeCheckbox()" onblur="showInputFieldTip(this)" />\
                    <input type="text" id="j_password2" name="j_password" value="Parola" class="login_password"\
                           onblur="showPasswordFieldTip(this, \'j_password2\', \'doLogin2\')"\
                           onfocus="hidePasswordFieldTip(this, \'j_password2\', \'doLogin2\');restoreRememberMeCheckbox()"\
                    />\
                    <div class="login_button" onclick="doLogin2()" id="loginButton2">Log-in</div>\
            </div>\
        </div>\
        <div class="underPanels" style="margin-top:88px; padding-left:7px;position:relative;">\
            <div id="master_sus_subpanel_login" style="display:none;margin-top:1px;">\
                <div style="position:relative;top:-3px;float:left;" id="master_sus_subpanel_acegi_security_remember_me_container"><input type="checkbox" name="_acegi_security_remember_me" id="master_sus_subpanel_acegi_security_remember_me" /></div>\
                <div style="position:relative;top:0px; float:left; color:#454545; ">Ţine-mă minte</div>\
            </div>\
            <div id="master_sus_subpanel_signup" style="display:none;margin-top:7px;">\
                <a href="http://www.hotnews.ro/inregistrare" target="_blank" style="color:#454545;">Înregistrează-te</a>\
            </div>\
            <div id="master_sus_subpanel_message" style="display:none;margin-top:1px;"></div>\
        </div>\
        <script type="text/javascript">selectTabLogin()</script>\
        </form>\
                ');
            } else {
//                window.location.href = '/';
            }
        }
    }

    new Ajax.Request(logoutUrl, opt);
    // Delete the cookies set by the IPB forum
    deleteCookie('member_id', '/');
    deleteCookie('member_id', '/', '.hotnews.ro');
    deleteCookie('session_id', '/');
    deleteCookie('session_id', '/', '.hotnews.ro');

    if($('loginFormHR')) {

    }
};
*/

function doLogout(logoutUrl) {
	var opt = {
		method: 'post',
		postBody: 'ajax=true',
		onSuccess: function(response) {
			if ($('master_sus_login') != null) {
				welcomeUsername = null;
				Element.update('master_sus_login', '\<div style="padding-left:20px;"><a href="javascript:showAjaxLoginForm(1);" ><img src="/images/new/login_header.jpg" /></a>\<div class="clearfloat spacer_5" style="font-size:6px;">&nbsp;</div><a href="javascript:showAjaxLoginForm(2);" style="padding-bottom:10px;"> <img src="/images/new/cont_hotnews.jpg" /></a><a href="/info-newsletter" style="padding:5px 0 10px 23px; background: url(/images/new/top/newsletter_over.gif) no-repeat 0 0; display: inline-block;"><span style="width:110px; color:#818181; display: inline-block; font-weight:normal;">Vreau newsletterul HotNews.ro</span></a></div>');
			}
		}
	}
	new Ajax.Request(logoutUrl, opt);
};

function doLogoutNew(logoutUrl) {
    $jh('#master_sus_login').append(
            '<div style="display: none"><iframe src="http://www.startupcafe.ro/logout"/></iframe></div>' +
            '<div style="display: none"><iframe src="http://www.hotnews.ro/logout"/></iframe></div>' +
            '<div style="display: none"><iframe src="http://www.webpr.ro/logout"/></iframe></div>' +
            '<div style="display: none"><iframe src="http://www.campusnews.ro/logout"/></iframe></div>');
    setTimeout('window.location.href = "http://sso.hotnews.ro/logout?service=" + window.location.href;', 300);

};

var tabs_api;
function attachEventsToLogin(){
    $jh(".colorbox-link").colorbox({inline:true, href:"#hotnews-login", opacity: 0.7, onClosed:function(){
        stopPollingUser = true;
    }});
    $jh("#hotnews-login-tabs").tabs(".hotnews-login-tab-panes > .pane");
    tabs_api = $jh("#hotnews-login-tabs").data("tabs");
    $jh(".hotnews-login-link").click(function() {
        tabs_api.click(0);
//          onLoginClick();
    });
    $jh(".hotnews-register-link").click(function() {
        tabs_api.click(1);
//          onLoginClick();
    });
    $jh(".hotnews-newsletter-link").click(function() {
        tabs_api.click(2);
//          onLoginClick();
    });
}

function showLoginForm(loginFormContainer, afterLoginGotoUrl, ifLoggedInShowHtml) {
    var containerElem = $(loginFormContainer);
    if (objIsValid(loginFormContainer) && objIsValid(containerElem)) {
        if (welcomeUsername == null) {
            // Not logged-in, so display the login form
            var loginPanel = $('loginPanel');
            if (objIsValid(loginPanel)) {
                containerElem.innerHTML = '';
                containerElem.appendChild(loginPanel);
                loginPanel.show();
            } else {
                Element.update(containerElem, '\
<div id="loginPanel" class="loginPanel">\
    <div class="loginTitle">Log-in</div>\
    <div class="loginContent">\
        <form action="/web_login_check" method="post" id="loginForm">\
            <div class="loginError"></div>\
            <div class="loginLine">\
                <div class="loginLine">\
                    <div style="float:right;">\
                        <input type="text" id="j_username" name="j_username" class="personal_imput"/>\
                    </div>\
                    <div class="loginLabel">Email</div>\
                </div>\
                <div>\
                    <div style="float:right">\
                        <input type="password" id="j_password" name="j_password" class="personal_imput"/>\
                        <script type="text/javascript">Event.observe($(\'j_password\'), "keypress",\
                         function(e){if(e.keyCode==Event.KEY_RETURN){doLoginGotoUrl(\'' + encodeURI(afterLoginGotoUrl).replace(/'/g, "\\'") + '\');}});</script>\
                    </div>\
                    <div class="loginLabel">Parola</div>\
                </div>\
            </div>\
            <div class="forgotPassword">\
                <a href="javascript:forgotPassword()">Am uitat parola</a>\
            </div>\
            <div class="loginRemember">\
                <div style="float:left;">\
                    <input type="checkbox" name="_acegi_security_remember_me"/>\
                </div>\
                <span>Ţine-mă minte timp de două săptămâni</span>\
            </div>\
            <div class="loginButtons">\
                <div class="loginLogin" onclick="doLoginGotoUrl(\'' + encodeURI(afterLoginGotoUrl).replace(/'/g, "\\'") + '\')">LOG-IN</div>\
                <div class="loginAbort" onclick="toggleLogin()">RENUNŢĂ</div>\
            </div>\
            <div id="loginMessage"></div>\
            <div class="loginBoxNewAccount">\
                <a class="login_link" href="/inregistrare">\
                    CONT NOU\
                </a>\
            </div>\
        </form>\
    </div>\
</div>');
                setTimeout(function() {$('loginPanel').show()}, 200);
            }
        } else {
            // Already logged-in, so display an optional html content
            if (objIsValid(ifLoggedInShowHtml) && !ifLoggedInShowHtml.blank()) {
                containerElem.update(ifLoggedInShowHtml);
            }
        }
    }
}

function doLoginGotoUrl(afterLoginGotoUrl) {
    var opt = {
        method: 'post',
        postBody: Form.serialize($('loginForm')) + '&ajax=true',
        onSuccess: function(response) {
            var msg = response.responseText;
            if (msg.startsWith("error:")) {
                Element.update('loginMessage', '<font color="red" align="center">Login invalid</font>');
                Element.show('loginMessage');
            } else if (msg.startsWith("url:")) {
                location.href = msg.substring("url:".length);
            } else if (msg.startsWith("success:")) {
                window.location.href = afterLoginGotoUrl;
            }
        }
    }
    new Ajax.Request('/web_login_check', opt);
}

/**
 * Handles the forgot your password button
 */
function forgotPassword() {
    var emailField = $('username');
    if (objIsValid(emailField)) {
        var emailAddress = emailField.value.strip();
        if (emailAddress.length == 0) {
            alert('Introduceti adresa de e-mail pentru a va putea recupera parola');
            emailField.focus();
        } else {
            userProxy.forgotPassword(emailAddress, function(response) {
                alert(response.obj);
            });
        }
    }
};

/**
 * Handles the subscribe to newsletter button
 */
function subscribeToNewsletter() {
    var emailField = $('signup_email');
    var master_sus_signup_button = $('master_sus_signup_button');
    if (objIsValid(emailField) && objIsValid(master_sus_signup_button)) {
        master_sus_signup_button.disabled = true;
        var emailAddress = emailField.value.strip();
        if (emailAddress.length == 0) {
            alert('Introduceti adresa de e-mail pentru a va abona la newsletter');
            emailField.focus();
        } else {
            userProxy.subscribeToNewsletter(emailAddress, function(response) {
                alert(response.obj);
                master_sus_signup_button.disabled = false;
            });
        }
    }
};

function subscribeToNewsletters() {
    var emailField = $('login-email');
    var nlGeneral = $('newsletter-ng').checked;
    var nlBreakingNews = $('newsletter-na').checked;
    var submitButton = $('newsletter-submit-btn');
    if (objIsValid(emailField) && objIsValid(submitButton)) {
        submitButton.disabled = true;
        var emailAddress = emailField.value.strip();
        if (emailAddress.length == 0) {
            alert('Introduceti adresa de e-mail pentru a va abona la newsletter!');
            emailField.focus();
            submitButton.disabled = false;
        } else if (nlGeneral == false && nlBreakingNews == false) {
            alert('Selectati cel putin un newsletter!');
            submitButton.disabled = false;
        } else {
            if (nlGeneral == true) {
                submitButton.disabled = true;
                userProxy.subscribeToNewsletter(emailAddress, function(response) {
                    alert(response.obj);
                    submitButton.disabled = false;
                });
            }
            if (nlBreakingNews == true) {
                submitButton.disabled = true;
                userProxy.subscribeToNewsletterBreakingNews(emailAddress, function(response) {
                    alert(response.obj);
                    submitButton.disabled = false;
                });
            }
        }
    }
}
;

/**
 * Selects the "Login" tab in the top-right corner
 */
function selectTabLogin() {
    var tabSignup = document.getElementById("master_sus_tab_signup");
    var tabLogin = document.getElementById("master_sus_tab_login");
    var panelSignup = document.getElementById("master_sus_panel_signup");
    var panelLogin = document.getElementById("master_sus_panel_login");
    var subpanelSignup = document.getElementById("master_sus_subpanel_signup");
    var subpanelLogin = document.getElementById("master_sus_subpanel_login");
    if (objIsValid(tabSignup) && objIsValid(tabLogin) && objIsValid(panelSignup) && objIsValid(panelLogin) && objIsValid(subpanelSignup) && objIsValid(subpanelLogin)) {
        tabSignup.className = "tab";
        panelSignup.style.display = "none";
        subpanelSignup.style.display = "none";
        tabLogin.className = "tab selected";
        panelLogin.style.display = "block";
        subpanelLogin.style.display = "block";
    }
}

/**
 * Selects the "Signup" tab in the top-right corner
 */
function selectTabSignup() {
    var tabSignup = document.getElementById("master_sus_tab_signup");
    var tabLogin = document.getElementById("master_sus_tab_login");
    var panelSignup = document.getElementById("master_sus_panel_signup");
    var panelLogin = document.getElementById("master_sus_panel_login");
    var subpanelSignup = document.getElementById("master_sus_subpanel_signup");
    var subpanelLogin = document.getElementById("master_sus_subpanel_login");
    if (objIsValid(tabSignup) && objIsValid(tabLogin) && objIsValid(panelSignup) && objIsValid(panelLogin) && objIsValid(subpanelSignup) && objIsValid(subpanelLogin)) {
        tabLogin.className = "tab";
        panelLogin.style.display = "none";
        subpanelLogin.style.display = "none";
        tabSignup.className = "tab selected";
        panelSignup.style.display = "block";
        subpanelSignup.style.display = "block";
    }
}

/**
 * Gets the GMT time
 */
function getGMTTime() {
    var now = new Date();
    // Offset to GMT (Greenwich Meridian Time)
    var offset = now.getTimezoneOffset() * 60000;
    return now.getTime() + offset;
};

/**
 * Computes the value for last updated
 */
function getLastUpdatedText(currentTime, lastUpdatedTime) {
    var diff = currentTime - lastUpdatedTime;
    if (diff < 0) {
        diff = 0;
    }
    diff = Math.floor(diff / 1000);
    var ore = Math.floor(diff / 3600);
    diff = diff % 3600;
    var minute = Math.floor(diff / 60);
    var secunde = diff % 60;
    var text = "";
    if (ore > 0) {
        text += ore + " or";
        text += (ore == 1) ? "a " : "e ";
    }
    if (minute > 0) {
        text += minute + " minut";
        text += (minute == 1) ? " " : "e ";
    } else {
        text += secunde + " secund";
        text += (secunde == 1) ? "a " : "e ";
    }
    return text;
};

/**
 * Validates register form
 */
function validateRegisterForm(pp) {
    var email = $F(pp + "_username").strip();
    var password = $F(pp + "_password").strip();
    var confirmPassword = $F(pp + "_confirm_password").strip();
    var firstname = $F(pp + "_firstname").strip();
    var lastname = $F(pp + "_lastname").strip();
    var nickname = $F(pp + "_nickname").strip();

    if (email.length == 0 || password.length == 0 || confirmPassword.length == 0
        || firstname.length == 0 || lastname.length == 0 || nickname.length == 0) {
        alert("Toate câmpurile sunt obligatorii!");
        return false;
    }
    if (!isValidEmail(email)) {
        alert("Nu ati introdus o adresa de email valida!");
        return false;
    }
    if (password.length < 5) {
        alert("Parola trebuie sa aiba cel putin 5 caractere!");
        return false;
    }
    if (password != confirmPassword) {
        alert("Parolele trebuie sa coincida!");
        return false;
    }
    var confirm = $("confirm");
    if (!confirm.checked) {
        alert("Pentru a va inregistra trebuie sa fiti de acord cu termenii si conditiile de utilizare! ");
        return false;
    }
    return true;
};


function isValidEmail(strEmail) {
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return filter.test(strEmail);
}

function validateUserInputForm(pp) {
    var email = $F(pp + "_email").strip();

    if (isValidEmail(email)) {
        alert("Emailul nu este valid");
        return false;
    }

    return true;
};

/**
 * Navigates through a multimedia strip
 */
function mmStripMove(mmStrip, increment, container, url, prefix) {
    var newFrom = parseInt(increment) + parseInt(mmStrip.from);
    var newTo = parseInt(increment) + parseInt(mmStrip.to);
    if (newFrom > 0 && newTo <= mmStrip.len) {
        new Ajax.Request(url + '?' + prefix + '__galleryId=' + mmStrip.galleryId
                + '&' + prefix + '__from=' + newFrom + '&' + prefix + '__to=' + newTo,
            {
                method: 'get',
                onSuccess: function(transport) {
                    var response = transport.responseText;
                    if (typeof(response != "undefined") && response.strip() != "") {
                        mmStrip.from = newFrom;
                        mmStrip.to = newTo;
                        $(container).update(response);
                    }
                },
                onFailure: function() {
                    alert('A aparut o problema pe server. Va rugam sa incercati mai tarziu.')
                }
            });
    }
};

/**
 * Shows the calendar for the archive
 */
function displayCalendar(container, params) {
    $(container).innerHTML = generateCalendar(params);
};

/**
 * Changes calendar date
 */
function changeCalendarDate(params, selectYearId, selectMonthId, cal) {
    var selectYear = $(selectYearId);
    var selectMonth = $(selectMonthId);
    var year = selectYear.options[selectYear.selectedIndex].value;
    var month = selectMonth.options[selectMonth.selectedIndex].value - 1;
    params.date = new Date(year, month, 1);
    displayCalendar(cal, params);
};

/**
 * Generates the calendar
 */
function generateCalendar(params) {
    // Default values for parameters
    if (params.date == null) {
        params.date = new Date();
    }
    if (params.monthNames == null) {
        params.monthNames = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    }
    if (params.displayMonthName == null) {
        params.displayMonthName = false;
    }
    if (params.weekDayNames == null) {
        params.weekDayNames = new Array('S', 'M', 'T', 'W', 'T', 'F', 'S');
    }
    if (params.displayWeekDayNames == null) {
        params.displayWeekDayNames = false;
    }
    if (params.firstDayInWeek == null) {
        params.firstDayInWeek = 0;
    }
    if (params.linkPrefix == null) {
        params.linkPrefix = "";
    }

    var day = params.date.getDate();
    var month = params.date.getMonth();
    var year = params.date.getFullYear();

    var this_month = new Date(year, month, 1);
    var next_month = new Date(year, month + 1, 1);

    var calendar_html = '<table cellspacing="0" cellpadding="1">';
    if (params.displayMonthName) {
        calendar_html += '<tr><td colspan="7">' + params.monthNames[month] + ' ' + year + '</td></tr>';
    }
    if (params.displayWeekDayNames) {
        calendar_html += '<tr>';
        for (var week_day = 0; week_day < 7; week_day++) {
            calendar_html += '<td class="weekdaynames">' + params.weekDayNames[week_day] + '</td>';
        }
        calendar_html += '</tr>';
    }

    // Find out when this month starts and ends.
    var first_week_day = this_month.getDay();
    first_week_day -= params.firstDayInWeek;
    if (first_week_day < 0)
        first_week_day = 6;
    var days_in_this_month = Math.round((next_month.getTime() - this_month.getTime()) / (1000 * 60 * 60 * 24));

    calendar_html += '<tr>';

    // Fill the first week of the month with the appropriate number of blanks.
    for (var week_day = 0; week_day < first_week_day; week_day++) {
        //    calendar_html += '<td style="background-color:9999cc; color:000000;"> </td>';
        calendar_html += '<td> </td>';
    }

    var week_day = first_week_day;
    for (var day_counter = 1; day_counter <= days_in_this_month; day_counter++) {
        week_day %= 7;
        if (week_day == 0)
            calendar_html += '</tr><tr>';

        var link;
        if (params.linkPrefix) {
            var month_ = new Number(month) + 1;
            //var date = day_counter + "_" + month_ + "_" + year;
            var date = year + "-" + (month_ < 10 ? "0" : "") + month_ + "-" + (day_counter < 10 ? "0" : "") + day_counter;
            link = "<a href='" + params.linkPrefix + date + "'>"
                + day_counter + "</a>";
        } else {
            link = day_counter;
        }
        // Do something different for the current day.
        if (day == day_counter)
        //      calendar_html += '<td style="text-align: center;"><b>' + day_counter + '</b></td>';
            calendar_html += '<td><b>' + link + '</b></td>';
        else
        //      calendar_html += '<td style="background-color:9999cc; color:000000; text-align: center;"> ' + day_counter + ' </td>';
            calendar_html += '<td> ' + link + ' </td>';

        week_day++;
    }

    calendar_html += '</tr>';
    calendar_html += '</table>';

    // Return the calendar.
    return calendar_html;
};

/**
 * Called when loading "arhiva_calendar_central"
 */
function hideAllArchiveCalendars() {
    for (var i = 1990; i < 2020; i++) {
        var c = document.getElementById("calendarForYear" + i);
        if (c) {
            Element.hide(c);
        }
    }
}

/**
 * Called when the user clicks on a calendar title in "arhiva_calendar_central"
 */
function clickedCalendarTitle(selectedYear) {
    var c = document.getElementById("calendarForYear" + selectedYear);
    Element.toggle(c);
/*
    for (var i = 1990; i < 2020; i++) {
        var c = document.getElementById("calendarForYear" + i);
        if (c) {
            if (i == selectedYear) {
                Element.show(c);
            } else {
                Element.hide(c);
            }
        }
    }
*/
}

/**
 * Validates quick search box
 */
function checkSearch() {
    var name = "quickSearchString";
    if ($(name).value.strip().length < 3 || !$(name).typedIn) {
        alert('Pentru a efectua cautarea, introduceti un cuvant cu cel putin trei litere');
        return false;
    }
    return true;
};

/**
 * Validates the quick search box and redirects to the search page
 */
function doSearch(siteUrl, searchPage, searchFieldName) {
    var searchString = $(searchFieldName).value.strip();
    if (searchString.length < 3 || !$(searchFieldName).typedIn) {
        alert('Pentru a efectua cautarea, introduceti un cuvant cu cel putin trei litere');
    } else {
        searchString = searchString.replace("/", " ");
        document.location.href = /*siteUrl +*/ "/" + searchPage + "/" + encodeURIComponent(searchString) + "/1";
    }
};

function doAdvancedSearch(siteUrl) {
    $('advancedSearchForm').submit();
}

/**
 * Opens up the print dialog page
 */
function printArticle(siteUrl, articleId) {
    window.open(siteUrl + "/print?articleId=" + articleId, "Printeaza", "height=800,width=800,resizable=yes,scrollbars=yes");
};

/**
 * Send article link via email
 */
function sendArticleLinkByMail(siteUrl, articleId, articleUrl) {
    var img = $("sendCount");
    if (img) {
        img.src = "";
        img.src = siteUrl + "/pageCount.htm?type=mail&articleId=" + articleId;
    }
    window.location.href = "mailto:?subject=Un articol interesant&body=" + articleUrl;
};

/**
 * Send article link via YM
 */
function sendArticleLinkByYM(siteUrl, articleId, articleUrl) {
    var img = $("sendCount");
    if (img) {
        img.src = "";
        img.src = siteUrl + "/pageCount.htm?type=ym&articleId=" + articleId;
    }
    window.location.href = "ymsgr:im?+&msg=" + articleUrl;
};

/**
 * Send link via email
 */
function sendLinkByMail(siteUrl, url) {
    var img = $("sendCount");
    if (img) {
        img.src = "";
        img.src = siteUrl + "/pageCount.htm?type=mail&url=" + url;
    }
    window.location.href = "mailto:?subject=O pagina interesanta&body=" + url;
};

/**
 * Send link via YM
 */
function sendLinkByYM(siteUrl, url) {
    var img = $("sendCount");
    if (img) {
        img.src = "";
        img.src = siteUrl + "/pageCount.htm?type=ym&url=" + url;
    }
    window.location.href = "ymsgr:im?+&msg=" + url;
};

/**
 * Decrease font size for article page
 */
function decreaseFontSize() {
    var articleContent = $("articleContent");
    if (articleContent) {
        var fontSize = articleContent.style.fontSize;
        var pos = fontSize.indexOf("px");
        if (pos > 0) {
            fontSize = fontSize.substr(0, pos);
            if (fontSize > MIN_FONT_SIZE) {
                fontSize = parseInt(fontSize) - parseInt(FONT_SIZE_STEP);
                articleContent.style.fontSize = fontSize + "px";
            }
        }
    }
}

/**
 * Increase font size for article page
 */
function increaseFontSize() {
    var articleContent = $("articleContent");
    if (articleContent) {
        var fontSize = articleContent.style.fontSize;
        var pos = fontSize.indexOf("px");
        if (pos > 0) {
            fontSize = fontSize.substr(0, pos);
            if (fontSize < MAX_FONT_SIZE) {
                fontSize = parseInt(fontSize) + parseInt(FONT_SIZE_STEP);
                articleContent.style.fontSize = fontSize + "px";
            }
        }
    }
}

/**
 * Inits comments container
 */
function initComments(addCommentFormContainer, elemComentariu, elemSizebox) {
    // aici initializam campul cu nr de caractere
    var comentariu = $(elemComentariu);
    if (comentariu && comentariu.value) {
        updateSizebox(elemSizebox, comentariu.value.length);
    }
    // aici afisam un element din pagina, daca a fost cerut explicit in URL
    var url = window.location.href;
    var pos = url.indexOf("#") + 1;
    if (pos > 0 && pos < url.length) {
        var urlFragment = url.substr(pos);
        if (urlFragment == "adaugaComentariu") {
            var formContainer = $(addCommentFormContainer);
            if (formContainer) {
                formContainer.style.display = "block";
                formContainer.style.visibility = "visible";
            }
            window.location.href = "#" + urlFragment;
        } else {
            var comentariu_ = "comentariu_";
            var len = comentariu_.length;
            if (urlFragment.substr(0, len) == comentariu_) {
                var commentId = urlFragment.substr(len + 1);
                expandComment(commentId);
                window.location.href = "#" + urlFragment;
            }
        }
    }
}

/**
 * Trims the comment value
 */
function updateSizebox(elemSizebox, textarea, size) {
    if (size > 2000) {
        //alert("EROARE! Comentariul contine " + size + " caractere si depaseste dimensiunea maxima de 2000 de caractere!");
        textarea.value = textarea.value.substr(0, 2000);
        return;
    }
    var sizebox = $(elemSizebox);
    if (sizebox) {
        sizebox.value = size + ' caractere scrise';
    }
}

/**
 * Collapses a comment
 */
function collapseComment(commentId) {
    var comment = $("co_" + commentId);
    if (comment) {
        comment.className = "commentCollapsed";
        comment.isCollapsed = true;
    }
}

/**
 * Expands a comment
 */
function expandComment(commentId) {
    var comment = $("co_" + commentId);
    if (comment) {
        comment.className = "cEx";
        comment.isCollapsed = false;
    }
}

/**
 * Toggles a comment
 */
function tc(commentId) {
    var comment = $("co_" + commentId);
    if (comment) {
        if (comment.className == "cEx") {
            comment.className = "cCo";
            comment.isCollapsed = true;
        } else {
            comment.className = "cEx";
            comment.isCollapsed = false;
        }
    }
}

/**
 * Shows the comment form as reply to another comment
 */
function rc(commentId, postLink/*, addCommentFormContainer, replyToCommentElem*/) {
    var addCommentFormContainer = 'addCommentFormContainer';
    var replyToCommentElem = 'replyToComment';
    //var postLink = $('pl' + commentId);

    var formContainer = $(addCommentFormContainer);
    if (formContainer) {
        if (postLink) {
            insertAfter(formContainer, postLink);
        }
        $$("#addCommentForm").each(function(elem) {elem.reset()});
        $("addCommentFormInnerContainer").show();
        formContainer.show();
    }
    var replyToComment = $(replyToCommentElem);
    if (replyToComment) {
        replyToComment.value = commentId;
    }
}

function rcOnlyLoggedIn(commentId, postLink) {
    if(objIsValid(welcomeUsername)) {
        rc(commentId, postLink);
    } else {
        var form = $('addCommentOnlyLoggedIn');
        if (form) {
            if (postLink) {
                insertAfter(form, postLink);
            }
            form.show();
        }
    }
}

/**
 * Sends a comment
 */
function sendComment(replyToComment, nume, email, notificaActivitate, subiect, comentariu, tokenId, param_ts, responseContainer, loadingMessage) {
    $("addCommentErrorMessage").hide();

    var rootObjectType = null;
    var rootObjectId = null;
    // Get the form values
    if (typeof(currentArticleId) != "undefined" && currentArticleId) {
        rootObjectType = 1;
        rootObjectId = parseInt(currentArticleId);
    }
    if (typeof(currentObjectType) != "undefined" && currentObjectType && typeof(currentObjectId) != "undefined" && currentObjectId) {
        rootObjectType = parseInt(currentObjectType);
        rootObjectId = parseInt(currentObjectId);
    }
    if (rootObjectType == null || rootObjectId == null) {
        return;
    }
    nume = $F(nume).strip();
    email = $F(email).strip();
    subiect = $F(subiect).strip();
    comentariu = $F(comentariu).strip();
    replyToComment = $F(replyToComment);
    notificaActivitate = $F(notificaActivitate);
    tokenId = $F(tokenId);
    var ts = param_ts.value;

    // Check if values are valid
    if (nume.length == 0 || email.length == 0 || subiect.length == 0 || comentariu.length == 0) {
        alert("Toate câmpurile sunt obligatorii!");
        return;
    }
    if (comentariu.length > 2000) {
        alert("EROARE! Comentariul contine " + comentariu.length + " caractere si depaseste dimensiunea maxima de 2000 de caractere!");
        return;
    }

    // Show the loading message
    var $responseContainer = $(responseContainer);
    $responseContainer.update(loadingMessage);
    $responseContainer.show();

/*
    // Send the AJAX request
    var parameters = {
        articleId: articleId,
        nume: nume,
        email: email,
        subiect: subiect,
        comentariu: comentariu,
        replyToComment: replyToComment,
        tokenId: tokenId,
        ts: ts
    };
    new Ajax.Updater(responseContainer, "/addComment.htm", {
        parameters: parameters
    });
*/

    // Send the request
    var parameters = {
        name: nume,
        email: email,
        authorNotifiedOnActivity: notificaActivitate,
        title: subiect,
        comment: comentariu,
        parentCommentStr: replyToComment
    };
    addCommentProxy.addComment(rootObjectType, rootObjectId, parameters, tokenId, ts, function(response) {
        $responseContainer.hide();

        if (response.status == 0) {
            $("addCommentForm").reset();
            $("addCommentFormInnerContainer").hide();
            $("addCommentSuccessMessage").show();
        } else {

            if (response.obj) {
                $("addCommentErrorMessageText").update(response.obj);
            }
            $("addCommentErrorMessage").show();
        }
    });
}

/**
 * Cancels a comment by hiding the comment window
 */
function cancelSendComment(addCommentFormContainer) {
    var formContainer = $(addCommentFormContainer);
    if (formContainer) {
        formContainer.hide();
    }
}

/**
 * Votes for a comment
 */
function vp(commentId) {
    commentVotingProxy.voteForComment(commentId, true, function(response) {
        if (response.status == 0) {
            var nrVotesSpan = $("v_" + commentId);
            if (nrVotesSpan) {
                var nrVotes = nrVotesSpan.innerHTML;
                if (nrVotes) {
                    nrVotes ++;
                    if (nrVotes > 0) {
                        nrVotes = "+" + nrVotes;
                    } else {
                        nrVotes = "" + nrVotes;
                    }
                    nrVotesSpan.innerHTML = nrVotes;
                }
            }
            var nrVotesTotalSpan = $("vt_" + commentId);
            var nrVotesTotalStrSpan = $("wt_" + commentId);
            if (nrVotesTotalSpan && nrVotesTotalStrSpan) {
                var nrVotesTotal = nrVotesTotalSpan.innerHTML;
                if (nrVotesTotal) {
                    nrVotesTotal ++;
                    nrVotesTotalSpan.innerHTML = nrVotesTotal;
                    if (nrVotesTotal == 1) {
                        nrVotesTotalStrSpan.innerHTML = "vot" ;
                    } else {
                        nrVotesTotalStrSpan.innerHTML = "voturi" ;
                    }
                }
            }

        } else {
            alert(response.obj);
            if (response.status != 5) {
                $jh.colorbox({inline:true, href:"#hotnews-login", opacity: 0.7});
                tabs_api.click(0);
 /*               var loginPanel = $('loginPanel');
                if (loginPanel) {
                    var postLink = $("co_" + commentId);
                    if (postLink) {
                        insertBefore(loginPanel, postLink);
                    }
                    loginPanel.show();
                    window.location.hash = "loginPanel";
                }    */
            }
        }
    });
}

/**
 * Votes against a comment
 */
function vc(commentId) {
    commentVotingProxy.voteForComment(commentId, false, function(response) {
        if (response.status == 0) {
            var nrVotesSpan = $("v_" + commentId);
            if (nrVotesSpan) {
                var nrVotes = nrVotesSpan.innerHTML;
                if (nrVotes) {
                    nrVotes --;
                    if (nrVotes > 0) {
                        nrVotes = "+" + nrVotes;
                    } else {
                        nrVotes = "" + nrVotes;
                    }
                    nrVotesSpan.innerHTML = nrVotes;
                }
            }
            var nrVotesTotalSpan = $("vt_" + commentId);
            var nrVotesTotalStrSpan = $("wt_" + commentId);
            if (nrVotesTotalSpan && nrVotesTotalStrSpan) {
                var nrVotesTotal = nrVotesTotalSpan.innerHTML;
                if (nrVotesTotal) {
                    nrVotesTotal ++;
                    nrVotesTotalSpan.innerHTML = nrVotesTotal;
                    if (nrVotesTotal == 1) {
                        nrVotesTotalStrSpan.innerHTML = "vot" ;
                    } else {
                        nrVotesTotalStrSpan.innerHTML = "voturi" ;
                    }
                }
            }

        } else {
            alert(response.obj);
            if (response.status != 5) {
                $jh.colorbox({inline:true, href:"#hotnews-login", opacity: 0.7});
                tabs_api.click(0);
     /*           var loginPanel = $('loginPanel');
                if (loginPanel) {
                    var postLink = $("co_" + commentId);
                    if (postLink) {
                        insertBefore(loginPanel, postLink);
                    }
                    loginPanel.show();
                    window.location.hash = "loginPanel";
                }      */
            }
        }
    });
}

/**
 * Vote for an object
 */
function vo(objectTypeId, objectId, votePro) {
    votingProxy.voteForObject(objectTypeId, objectId, votePro, function(response) {
        if (response.status == 0) {
            var nrVotesSpan = $('nrVotes');
            if (nrVotesSpan) {
                var nrVotes = nrVotesSpan.innerHTML;
                if (nrVotes) {
                    if (votePro) {
                        nrVotes += votePro;
                    } else {
                        nrVotes += votePro;
                    }
                    if (nrVotes > 0) {
                        nrVotes = "+" + nrVotes;
                    } else {
                        nrVotes = "" + nrVotes;
                    }
                    nrVotesSpan.innerHTML = nrVotes;
                }
                var nrVotesTotalSpan = $("vt");
                var nrVotesTotalStrSpan = $("wt");
                if (nrVotesTotalSpan && nrVotesTotalStrSpan) {
                    var nrVotesTotal = nrVotesTotalSpan.innerHTML;
                    if (nrVotesTotal) {
                        nrVotesTotal ++;
                        nrVotesTotalSpan.innerHTML = nrVotesTotal;
                        if (nrVotesTotal == 1) {
                            nrVotesTotalStrSpan.innerHTML = "vot" ;
                        } else {
                            nrVotesTotalStrSpan.innerHTML = "voturi" ;
                        }
                    }
                }
            } else {
                var msg = $('ratingMsg');
                if(msg) {
                    msg.innerHTML = 'Votul dvs a fost salvat';
                }
            }

        } else {
            alert(response.obj);
            if (response.status != 5) {
                var loginPanel = $('loginPanel');
                if (loginPanel) {
                    var postLink = null;
                    if (postLink) {
                        insertBefore(loginPanel, postLink);
                    }
                    loginPanel.show();
                    if (postLink) {
                        window.location.hash = 'loginPanel';
                    }
                }
            }
        }
    });
}

function moderateContestItemForFinale(itemOnPage, itemId, approve) {
    votingProxy.moderateContestItemForFinale(itemId, approve, function(response) {
        if (response.status == 0) {
            var galleryItem = $("galleryItem" + itemOnPage);
            galleryItem.style.backgroundColor = (response.obj ? "limegreen" : "red");
        } else {
            alert(response.obj);
        }
    });
}

/**
 * @Deprecated
 */
function getSecureToken(warning, url, param_ts) {
    $(warning).remove();
    new Ajax.Request(url, {
        method: 'get',
        onSuccess: function(transport) {
            param_ts.value = "" + transport.responseText;
        },
        onFailure: function() {
            alert('A aparut o problema pe server. Va rugam sa incercati mai tarziu.')
        }
    });
}

/**
 * Opens a centered popup
 */
function openCenteredPopup(url, name, w, h) {
    var int_windowLeft = (screen.width - w) / 2;
    var int_windowTop = (screen.height - h) / 2;
    var str_windowProperties = 'height=' + h + ',width=' + w + ',top=' + int_windowTop + ',left=' + int_windowLeft + ',scrollbars=' + false + ',resizable=' + true + ',menubar=' + false + ',toolbar=' + false + ',location=' + false + ',statusbar=' + false + ',fullscreen=' + false + '';
    var obj_window = window.open(url, name, str_windowProperties);
    if (parseInt(navigator.appVersion) >= 4) {
        obj_window.window.focus();
    }
}

/**
 * Opens zoom version for a picture
 */
function openZoom(zoomUrl, imageUrl, width, height) {
    var int_windowLeft = (screen.width - width) / 2;
    var int_windowTop = (screen.height - height) / 2;
    var newWindow = window.open(
            zoomUrl + '&imgUrl=' + imageUrl,
        'ZOOM',
            'scrollbars=yes,left=' + int_windowLeft + ',top=' + int_windowTop + ',width=' + width + ',height=' + height + ',resizable=1,toolbar=0,location=0,directories=0,status=0,menubar=0,copyhistory=0'
    );
    if (document.images) {
        newWindow.focus();
    }
}

/**
 * Opens an url in a new page
 */
function targetBlank(url) {
    var blankWin = window.open(
        url,
        '_blank',
        'menubar=yes,toolbar=yes,location=yes,directories=yes,fullscreen=no,titlebar=yes,hotkeys=yes,status=yes,scrollbars=yes,resizable=yes'
    );
    if (document.images) {
        blankWin.focus();
    }
    return false;
}

/**
 * Handles gallery page submit form
 */
function onSubmitSearchGallery() {
    var searchField = $('search');
    searchField.value = searchField.value.strip();
    return true;
}

/**
 * Initializes the tv guide
 */
function initTV() {
    var elems = document.getElementsByClassName("tvItem");
    if (elems && elems.length > 0) {
        var pos = [parseInt(Math.random() * elems.length)];
        while(pos.length < 5) {
            var retries = 0;
            var newPos = 0;
            while(retries < 100) {
                newPos = parseInt(Math.random() * elems.length);
                var found = false;
                for(var i = 0; i < pos.length; i++) {
                    if(pos[i] == newPos) {
                        found = true;
                    }
                }
                if(!found) {
                    break;
                }
                retries ++;
            }
            pos[pos.length] = newPos;
        }
        for(var i = 0; i < pos.length; i++) {
            $("tv" + i).update(elems[pos[i]].innerHTML);
        }
    }
}

/**
 * Switches the image for pozilla
 */
function changePozilla(idx) {
    if($) {
        $("pozillaMain").innerHTML = $("pozillaMain_" + idx).innerHTML;
        $("pozillaText").innerHTML = $("pozillaText_" + idx).innerHTML;
    }
}

/**
 * Inits the quote gallery
 */
function initQuoteGallery(suffix) {
    if($) {
        var qge = $("quoteGalleryElems"+suffix);
        if(!qge) {} else {
            var quoteGalleryItems = qge.getElementsByClassName("quoteGalleryItem");
            if (quoteGalleryItems && quoteGalleryItems.length > 0) {
                var pos = parseInt(Math.random() * quoteGalleryItems.length);
                $("quoteGalleryPlaceHolder"+suffix).update(quoteGalleryItems[pos].innerHTML);
            }
        }
    }
}

/**
 * Checks whether an object is valid or not (not null, not undefined, etc)
 *
 * @param obj the object to be checked
 * @return true if is valid, false otherwise
 */
function objIsValid(obj) {
    return !(!obj || obj == null || typeof(obj) == "undefined" || obj == "null");
}


/**
 * Trim functionality for strings
 * @param str the string to be trimmed
 */
function trim(/* string */str){
	if(!str.replace){ return str; }
	if(!str.length){ return str; }
	var re = (/^\s+|\s+$/g);
	return str.replace(re, "");
}

function toggleHoroscop(elemName) {
    var x, y;
    if (typeof window.pageXOffset != 'undefined') {
        x = window.pageXOffset;
        y = window.pageYOffset;
    } else {
        if ((!window.document.compatMode) || (window.document.compatMode == 'BackCompat')) {
            x = window.document.body.scrollLeft;
            y = window.document.body.scrollTop;
        } else {
            x = window.document.documentElement.scrollLeft;
            y = window.document.documentElement.scrollTop;
        }
    }
    $(elemName).toggle();
    window.scrollTo(x, y);
}

function phbzoomofs() {
	return 32;
}

function phbzoomofsx() {
	return phbzoomofs();
}

function phbzoomofsy() {
	return phbzoomofs()+10;
}

function preselectDirectAccessHotnewsRapid() {
    var acces_direct_da = document.getElementById("acces_direct_da");
    var acces_direct_nu = document.getElementById("acces_direct_nu");
    if (acces_direct_da && acces_direct_nu) {
        var accesRapid = getCookie("hn_acces_rapid", false);
        if (accesRapid == "HotNewsRapid") {
            acces_direct_da.checked = true;
            acces_direct_nu.checked = false;
        } else {
            acces_direct_da.checked = false;
            acces_direct_nu.checked = true;
        }
    }
}

function Tab(title, imgNormalStart, cssNormal, width) {
    this.title = title;
    this.width = null;
    var start = "/images/new/top";
    if(objIsValid(imgNormalStart)) {
        this.imgNormal = start + imgNormalStart + ".gif";
    } else {
        this.imgNormal = null;
    }
    if(objIsValid(imgNormalStart)) {
        this.imgOver = start + imgNormalStart + "_over.gif";
    } else {
        this.imgSelected = null;
    }
    if(objIsValid(imgNormalStart)) {
        this.imgSelected = start + imgNormalStart + "_over.gif";
    } else {
        this.imgSelected = null;
    }
    if(objIsValid(width)) {
        this.width = width;
    }
    this.cssNormal = cssNormal;
    this.cssOver = cssNormal + "_over";
    this.cssSelected = cssNormal + "_selected";
}
function SimpleToolbar(prefix, width, tools) {
    if (typeof prefix == "string" && tools instanceof Array) {
        this.width = width;
        this.prefix = prefix;
        this.tools = tools;
    }
}
SimpleToolbar.prototype.createHtmlToolbar = function(toolbarVarName) {
    if (typeof toolbarVarName == "string" && typeof this.prefix == "string" && this.tools instanceof Array) {
        var s = "";
        var margin = 0;
        for (var i = 0; i < this.tools.length; i++) {
            var tool = this.tools[i];
            if (tool instanceof Tab && tool.title != null) {
                s += "<div id=\"" + this.prefix + "_tool" + i + "\" style='margin-left:" + margin  + "px;" +
                    (tool.width != null ? "width:" + tool.width : "") + "px;' class=\"tool " +
                    (tool.cssNormal ? tool.cssNormal : "") + "\" onclick=\"" + toolbarVarName + ".selectTool(" + i + ")\"" + ">";
                if(tool.imgNormal != null) {
                    s += "<img id=\"" + this.prefix + "_toolImg" + i + "\"";
                    s += " src=\"" + (tool.imgNormal ? tool.imgNormal : "")+ "\"";
                    s += " alt=\"" + (tool.title ? tool.title : "") + "\"";
                    s += " title=\"" + (tool.title ? tool.title : "") + "\"";
                    s += " onmouseover=\"" + toolbarVarName + ".hilightTool(" + i + ")\"";
                    s += " onmouseout=\"" + toolbarVarName + ".unhilightTool(" + i + ")\"";
                    //s += " onclick=\"" + toolbarVarName + ".selectTool(" + i + ")\"";
                    s += " />";
                } else {
                    s += tool.title;
                }
                s += "</div>";
                margin += tool.width != null ? tool.width : this.width;
            }
        }
        return s;
    }
    return "";
};
SimpleToolbar.prototype.selectTool = function(i) {
    if (this.tools instanceof Array && typeof i == "number" && i < this.tools.length) {
        // Deselect the old selected tool, if any
        if (this.selectedTool != null && this.selectedTool < this.tools.length) {
            var oldTool = this.tools[this.selectedTool];
            if (oldTool instanceof Tab) {
                var toolDiv = document.getElementById(this.prefix + "_tool" + this.selectedTool);
                var toolImg = null;
                if(oldTool.imgNormal != null) {
                    toolImg = document.getElementById(this.prefix + "_toolImg" + this.selectedTool);
                }
                var toolPanel = document.getElementById(this.prefix + "_panel_" + this.selectedTool);
                if (toolDiv && toolPanel) {
                    if (oldTool.cssNormal) {
                        toolDiv.className = "tool " + oldTool.cssNormal;
                    }
                    if(oldTool.imgNormal != null) {
                        if (oldTool.imgNormal) {
                            toolImg.src = oldTool.imgNormal;
                        }
                    }
                    toolPanel.style.display = "none";
                }
                this.selectedTool = null;
            }
        }
        // Select the new tool
        var tool = this.tools[i];
        if (tool instanceof Tab) {
            var toolDiv = document.getElementById(this.prefix + "_tool" + i);
            var toolImg = null;
            if(tool.imgNormal != null) {
                toolImg = document.getElementById(this.prefix + "_toolImg" + i);
            }
            var toolPanel = document.getElementById(this.prefix + "_panel_" + i);
            if (toolDiv && toolPanel) {
                if (tool.cssSelected) {
                    toolDiv.className = "tool " + tool.cssSelected;
                }
                if(tool.imgNormal != null) {
                    if (tool.imgSelected) {
                        toolImg.src = tool.imgSelected;
                    }
                }
                toolPanel.style.display = "";
            }
            this.selectedTool = i;
        }
    }
};
SimpleToolbar.prototype.hilightTool = function(i) {
    if (this.tools instanceof Array && typeof i == "number" && i < this.tools.length && i != this.selectedTool) {
        var tool = this.tools[i];
        var toolDiv = document.getElementById(this.prefix + "_tool" + i);
        var toolImg = document.getElementById(this.prefix + "_toolImg" + i);
        if (tool instanceof Tab && toolDiv && toolImg) {
            if (tool.cssOver) {
                toolDiv.className = "tool " + tool.cssOver;
            }
            if (tool.imgOver) {
                toolImg.src = tool.imgOver;
            }
        }
    }
};
SimpleToolbar.prototype.unhilightTool = function(i) {
    if (this.tools instanceof Array && typeof i == "number" && i < this.tools.length && i != this.selectedTool) {
        var tool = this.tools[i];
        var toolDiv = document.getElementById(this.prefix + "_tool" + i);
        var toolImg = document.getElementById(this.prefix + "_toolImg" + i);
        if (tool instanceof Tab && toolDiv && toolImg) {
            if (tool.cssNormal) {
                toolDiv.className = "tool " + tool.cssNormal;
            }
            if (tool.imgNormal) {
                toolImg.src = tool.imgNormal;
            }
        }
    }
};

function checkTextAreaMaxLength(textarea, maxlength) {
    if (textarea.value.length > maxlength) {
        textarea.value = textarea.value.substr(0, maxlength);
    }
}

function getFormValues(form) {
    var values = {};
    for (var i = 0; i < form.elements.length; i++) {
        var elem = form[i];
        if (elem.name != null && elem.value != null) {
            if (values[elem.name] == null) {
                values[elem.name] = [];
            }
            values[elem.name].push(elem.value);
        }
    }
    return values;
}

function showWeather(selected) {
    //alert("set cookie " + selected);
    // Delete all subdomain cookies
    if (typeof deleteCookie != undefined) {
        deleteCookie("hn_weather", "/", "www.hotnews.ro");
        deleteCookie("hn_weather", "/", "economie.hotnews.ro");
        deleteCookie("hn_weather", "/", "sport.hotnews.ro");
        deleteCookie("hn_weather", "/", "life.hotnews.ro");
        deleteCookie("hn_weather", "/", "revistapresei.hotnews.ro");
        deleteCookie("hn_weather", "/", "forum.hotnews.ro");
        deleteCookie("hn_weather", "/", "english.hotnews.ro");
        deleteCookie("hn_weather", "/", "student.hotnews.ro");
    }
    // Set only the master cookie
    setCookie("hn_weather", selected, 365, "/", ".hotnews.ro");
    //alert("done");
    if (document.getElementById("weatherLink")) {
        // Vremea Stanga
        document.getElementById("weatherLink").href = 'http://weather.yahoo.com/forecast/ROXX00' + weatherCities[selected].l;
        //document.getElementById("weatherImage").src = "/images/new/vreme/" + weatherCities[selected].td + ".gif";
        //document.getElementById("weatherImage").alt = weatherDesc[weatherCities[selected].td];
        //document.getElementById("weatherToday").innerHTML = weatherDesc[weatherCities[selected].td];
        document.getElementById("weatherImage").src = weatherCities[selected].imn;
        document.getElementById("weatherImage").alt = weatherCities[selected].dn;
        document.getElementById("weatherToday").innerHTML = weatherCities[selected].dn;
        //document.getElementById("tempNow").innerHTML = weatherCities[selected].tn + "&#176;";
        document.getElementById("lowTempToday").innerHTML=weatherCities[selected].ltd  + "&#176;C";
        document.getElementById("highTempToday").innerHTML = weatherCities[selected].htd + "&#176;C";
        document.getElementById("weatherTomorrow").innerHTML = weatherCities[selected].dt;
        document.getElementById("lowTempTomorrow").innerHTML = weatherCities[selected].ltm  + "&#176;C";
        document.getElementById("highTempTomorrow").innerHTML = weatherCities[selected].htm  + "&#176;C";
    }
    if (document.getElementById("top_weatherCity")) {
        // Vremea sus
        document.getElementById("top_weatherCity").innerHTML = weatherCities[selected].c;
        document.getElementById("top_weatherLinkToday").href = 'http://weather.yahoo.com/forecast/ROXX00' + weatherCities[selected].l;
        //document.getElementById("top_weatherImageToday").src = "/images/new/top/vreme_azi/" + weatherCities[selected].td + ".gif";
        //document.getElementById("top_weatherImageToday").alt = weatherDesc[weatherCities[selected].td];
        //document.getElementById("top_weatherToday").innerHTML = weatherDesc[weatherCities[selected].td];
        document.getElementById("top_weatherImageToday").src =  weatherCities[selected].imn;
        document.getElementById("top_weatherImageToday").alt = weatherCities[selected].dn;
        document.getElementById("top_weatherToday").innerHTML = weatherCities[selected].dn;
        document.getElementById("top_tempNow").innerHTML = weatherCities[selected].tn  + "&#176;";
        document.getElementById("top_lowTempToday").innerHTML = weatherCities[selected].ltd  + "&#176;C";
        document.getElementById("top_highTempToday").innerHTML = weatherCities[selected].htd  + "&#176;C";
        //TOMORROW
        document.getElementById("top_weatherLinkTomorrow").href = 'http://weather.yahoo.com/forecast/ROXX00' + weatherCities[selected].l;
        document.getElementById("top_weatherImageTomorrow").src = weatherCities[selected].imt;
        document.getElementById("top_weatherImageTomorrow").alt = weatherCities[selected].dt;
        //document.getElementById("top_weatherTomorrow").innerHTML = weatherDesc[weatherCities[selected].tm];
        document.getElementById("top_lowTempTomorrow").innerHTML = weatherCities[selected].ltm  + "&#176;C";
        document.getElementById("top_highTempTomorrow").innerHTML = weatherCities[selected].htm  + "&#176;C";
    }
}

function onlyOneValue(toUncheck, toCheck){
    $jh('[id^="' + toUncheck + '"]').removeAttr("checked");
    $jh('#' + toCheck).attr('checked', 'checked');
}

function submitPollAnswers(pollId, loadingMessage) {
    if (objIsValid(pollId)) {
        var loadingMessageContainer = $("loadingMessage_" + pollId);
        loadingMessageContainer.hide();

        // Validate the poll answers selected
        var formValues = DWRUtil.getValues("pollForm_" + pollId);
        var poll = window.polls[pollId];
        for (var i = 1; i < poll.questions.length; i++) {
            var question = poll.questions[i];
            var valid = false;
            for (var j = 1; j < question.answers.length; j++) {
                var answer = question.answers[j];
                var crtAnswerId = "poll_answer_" + poll.id + "_" + question.position + "_" + answer.position;
                var answerSelected = formValues[crtAnswerId];
                if (answerSelected != null) {
                    answer.selected = answerSelected;
                    if (answerSelected) {
                        if(objIsValid($("poll_answer_free_" + poll.id + "_" + question.position + "_" + answer.position))) {
                            var text = formValues["poll_answer_free_" + poll.id + "_" + question.position + "_" + answer.position];
                            if(text == '') {
                                valid = false;
                            } else {
                                valid = true;
                            }
                        } else {
                            valid = true;
                        }
                    }
                }
            }
            if (!valid) {
                alert("Trebuie sa selectati/completati cel putin cate un raspuns pentru fiecare intrebare!");
                return false;
            }
        }

        // Show the loading message
        loadingMessageContainer.update(loadingMessage);
        loadingMessageContainer.show();

        // Send the request
        pollProxy.submitPollAnswers(pollId, formValues, function(response) {
            loadingMessageContainer.hide();
            if(pollId == 356) {
                $("uif356").hide();
            }
            if (response.status == 1) {
                alert("Trebuie sa selectati cel putin cate un raspuns pentru fiecare intrebare!");
            } else if ((response.status == 0 || response.status == 5 || response.status == 6 ||
                response.status == 3 || response.status == 7 || response.status == 8 || response.status == 9) && response.obj != null) {
                if (response.status == 5) {
                    alert("Ati votat deja pentru acest sondaj!\nDaca sunteti sigur ca nu ati mai votat, " +
                        "atunci inregistrati-va in contul dumneavoastra si incercati din nou sa votati.");
                } else if (response.status == 6) {
                    alert(response.obj);
                    return;
                }
                var pollIds = getCookie("hn_poll_votes", '').split(' ');
                var add = true;
                for (var i = 0, len = pollIds.length; i < len; i++) {
                    if (pollId == pollIds[i]) {
                        add = false;
                        break;
                    }
                }
                if (add) {
                    pollIds.push(pollId);
                }
                setCookie("hn_poll_votes", pollIds.join(' '), 365, '/', '.hotnews.ro');

                if (response.status == 7) {
                    if(pollId==10979330){
                        $("poll_questions_"+pollId).update("<hr/>Felicitari, ai raspuns corect la toate intrebarile! Lasa-ne adresa ta de e-mail unde sa te contactam daca vei fi castigator prin tragere la sorti!");
                    } else {
                        $("poll_questions_"+pollId).update(response.obj);
                    }

                    if(pollId==10979330){
                        $("uif356").show();
                    }
                } else if (response.status == 8) {
                    loadingMessageContainer.update(response.obj);
                    loadingMessageContainer.show();
                    if(pollId == 356) {
                        $("uif356").show();
                    }
                } else if (response.status == 9) {
                    loadingMessageContainer.update(response.obj);
                    loadingMessageContainer.show();
                } else if (response.status == 3) {
                    if(pollId==10979330){
                        $("poll_questions_"+pollId).update("<hr/> Nu ai raspuns corect la toate intrebarile. Citeste despre IPTV <a href='http://economie.hotnews.ro/stiri-economie-10979808-regulament-concurs-iptv.htm' style='color:blue;text-decoration:underline;'>aici</a> si mai incearca!");
                    } else {
                        showPollCorrectAnswers(pollId,response.obj);
                    }
                } else{
                    // Show the results
                    showPollResultsAfterSubmit(pollId, response.obj);
                }
            } else if (response.status == 5) {
                alert("Ati votat deja pentru acest sondaj!\nDaca sunteti sigur ca nu ati mai votat, atunci inregistrati-va in contul dumneavoastra si incercati din nou sa votati.");
            } else {
                alert("Ne pare rau, a aparut o eroare pe server. Va rugam sa incercati mai tarziu.");
            }
            return (response.status == 0);
        });
    }
    return false;
}

function getPollResults(pollId) {
    if (objIsValid(pollId)) {
        pollProxy.getPollResults(pollId, function(response) {
            if (response.status == 0 && response.obj != null) {
                // Show the results
                showPollResults(pollId, response.obj);
            } else if (response.status == 3) {
                alert("Trebuie sa votati pentru a putea vedea rezultatele sondajului!\nDaca sunteti sigur ca ati votat deja, atunci inregistrati-va in contul dumneavoastra \n si incercati din nou sa vedeti rezultatele.");
            } else {
                alert("Ne pare rau, a aparut o eroare pe server. Va rugam sa incercati mai tarziu.");
            }
        });
    }
}

function showPollResults(pollId, pollResults) {
    var poll = window.polls[pollId];
    var htmlResults = '<div class="answerResults">';
    // pollQuestionsSorted will start at 0 because of the sort
    var pollQuestionsSorted = [];
    for (var i = 1; i < poll.questions.length; i++) {
        pollQuestionsSorted[i-1] = poll.questions[i];
    }
    pollQuestionsSorted.sort(function(a,b) {
        return a.position - b.position;
    });
    for (var i = 0; i < pollQuestionsSorted.length; i++) {
        var question = pollQuestionsSorted[i];
        htmlResults += '<div class="pollQuestion">';
        htmlResults += '<div class="pollQuestionTitle">' + question.question + '</div>';
        htmlResults += '<div class="pollQuestionType">' + (question.multipleAnswers ? '(Mai multe răspunsuri posibile)': '(Un singur răspuns posibil)') + '</div>';
        htmlResults += '<ul class="pollQuestionAnswersResults">';
        // Count the total votes for this question
        var totalVotesForQuestion = 0;
        // question.answers starts at 1, and has length = nr_answers + 1
        for (var j = 1; j < question.answers.length; j++) {
            var answer = question.answers[j];
            var answerKey = "poll_answer_" + poll.id + "_" + question.position + "_" + answer.position;
            totalVotesForQuestion += pollResults[answerKey];
        }
        // Display the results
        // question.answers starts at 1, and has length = nr_answers + 1
        for (var j = 1; j < question.answers.length; j++) {
            var answer = question.answers[j];
            var answerKey = "poll_answer_" + poll.id + "_" + question.position + "_" + answer.position;
            htmlResults += '<li class="pollQuestionAnswerResult"><div class="answer">' + answer.answer;
            if(pollId == 7182732) {
                htmlResults += '</div> <div class="votes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>';
            } else {
                htmlResults += '</div> <div class="votes">[' + pollResults[answerKey] /*+ ' vot' + (pollResults[answerKey] != 1 ? 'uri' : '')*/ + ']</div>';
            }
            var percentVotes = Math.round(100 * pollResults[answerKey] / totalVotesForQuestion);
            var percentVotesS = Math.round(1000 * pollResults[answerKey] / totalVotesForQuestion) / 10;
            htmlResults += '<div class="bar">';
            if (percentVotes > 0) {
                htmlResults += '<img src="/images/new/sondaj/bar_left.gif" alt=""/><img src="/images/new/sondaj/bar.gif" alt="" height="11" width="' + percentVotes + '"/><img src="/images/new/sondaj/bar_right.gif" alt=""/>';
            }
            htmlResults += '</div> <div class="percentVotes">[' + percentVotesS + '%]</div></li>';
        }

        if(pollId == 7182732) {
            htmlResults += '<li class="pollQuestionTotalVotes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li></ul></div>';
        } else {
            htmlResults += '<li class="pollQuestionTotalVotes">Total voturi: ' + totalVotesForQuestion + '</li></ul></div>';
        }
    }
    htmlResults += '</div>';
    $("poll_questions_" + pollId).update(htmlResults);
}

function showPollResultsAfterSubmit(pollId, pollResults) {
    var poll = window.polls[pollId];
    var htmlResults = '<div class="answerResults">';
    // pollQuestionsSorted will start at 0 because of the sort
    if(poll.linkToAnswer != '' && poll.linkToAnswer != 'null') {
        htmlResults += '<div class="pollQuestion">';
        htmlResults += '<div class="pollQuestionCorrect" style="font-size:1.2em;color:red;">Aflati solutia ' +
            '<a href="' + poll.linkToAnswer + '">aici</a>.</div>';
        htmlResults += '<ul class="pollQuestionAnswersResults">';
    } else{
        var pollQuestionsSorted = [];
        for (var i = 1; i < poll.questions.length; i++) {
            pollQuestionsSorted[i-1] = poll.questions[i];
        }
        pollQuestionsSorted.sort(function(a,b) {
            return a.position - b.position;
        });
        for (var i = 0; i < pollQuestionsSorted.length; i++) {
            var question = pollQuestionsSorted[i];
            htmlResults += '<div class="pollQuestion">';
            if(pollResults[0]) {
                htmlResults += '<div class="pollQuestionCorrect" style="font-size:1.2em;">Varianta corecta este:</div>';
            }
            htmlResults += '<div class="pollQuestionTitle">' + question.question + '</div>';
            htmlResults += '<div class="pollQuestionType">' + (question.multipleAnswers ? '(Mai multe răspunsuri posibile)': '(Un singur răspuns posibil)') + '</div>';
            htmlResults += '<ul class="pollQuestionAnswersResults">';
            if(pollResults[0]) {
                var correctAnswers = pollResults[1][question.id];
                if(typeof(correctAnswers) != 'undefined' && correctAnswers != null) {
                    for(var j = 0; j < correctAnswers.length; j++) {
                        htmlResults += '<li class="pollQuestionAnswerResult"><div class="answer" style="float: none;">' +
                            correctAnswers[j] +
                            '</div></li>';
                    }
                } else {
                    alert("A intervenit o eroare la afisarea rezultatelor");
                }
            } else {
                // Count the total votes for this question
                var totalVotesForQuestion = 0;
                // question.answers starts at 1, and has length = nr_answers + 1
                for (var j = 1; j < question.answers.length; j++) {
                    var answer = question.answers[j];
                    var answerKey = "poll_answer_" + poll.id + "_" + question.position + "_" + answer.position;
                    totalVotesForQuestion += pollResults[1][answerKey];
                }
                // Display the results
                // question.answers starts at 1, and has length = nr_answers + 1
                for (var j = 1; j < question.answers.length; j++) {
                    var answer = question.answers[j];
                    var answerKey = "poll_answer_" + poll.id + "_" + question.position + "_" + answer.position;
                    htmlResults += '<li class="pollQuestionAnswerResult"><div class="answer">' + answer.answer;
                    if(pollId == 7182732) {
                        htmlResults += '</div> <div class="votes">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>';
                    } else {
                        htmlResults += '</div> <div class="votes">[' + pollResults[1][answerKey] /*+ ' vot' + (pollResults[1][answerKey] != 1 ? 'uri' : '')*/ + ']</div>';
                    }
                    var percentVotes = Math.round(100 * pollResults[1][answerKey] / totalVotesForQuestion);
                    var percentVotesS = Math.round(1000 * pollResults[1][answerKey] / totalVotesForQuestion) / 10;
                    htmlResults += '<div class="bar">';
                    if (percentVotes > 0) {
                        htmlResults += '<img src="/images/new/sondaj/bar_left.gif" alt=""/><img src="/images/new/sondaj/bar.gif" alt="" height="11" width="' + percentVotes + '"/><img src="/images/new/sondaj/bar_right.gif" alt=""/>';
                    }
                    htmlResults += '</div> <div class="percentVotes">[' + percentVotesS + '%]</div></li>';
                }
                if(pollId == 7182732) {
                    htmlResults += '<li class="pollQuestionTotalVotes"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li></ul>';
                } else {
                    htmlResults += '<li class="pollQuestionTotalVotes">Total voturi: ' + totalVotesForQuestion + '</li></ul>';
                }
            }
            htmlResults += '</div>';
        }
    }
    htmlResults += '</div>';
    $("poll_questions_" + pollId).update(htmlResults);
}

function showPollCorrectAnswers(pollId, pollResults) {
    var poll = window.polls[pollId];
    var htmlResults = '<div class="answerResults">';
    // pollQuestionsSorted will start at 0 because of the sort
    var pollQuestionsSorted = [];
    for (var i = 1; i < poll.questions.length; i++) {
        pollQuestionsSorted[i-1] = poll.questions[i];
    }
    pollQuestionsSorted.sort(function(a,b) {
        return a.position - b.position;
    });
    for (var i = 0; i < pollQuestionsSorted.length; i++) {
        var question = pollQuestionsSorted[i];
        if(poll.linkToAnswer != '' && poll.linkToAnswer != 'null') {
            htmlResults += '<div class="pollQuestion">';
            htmlResults += '<div class="pollQuestionCorrect" style="font-size:1.2em;color:red;">Raspunsul dumneavoastra nu a fost corect. Aflati solutia ' +
                '<a href="' + poll.linkToAnswer + '">aici</a>.</div>';
            htmlResults += '<ul class="pollQuestionAnswersResults">';
        } else{
            var answers = pollResults[1][question.id];
            if(answers) {
                htmlResults += '<div class="pollQuestion">';
                htmlResults += '<div class="pollQuestionCorrect" style="font-size:1.2em;color:red;">Raspunsul dumneavoastra nu a fost corect. Varianta corecta este:</div>';
                htmlResults += '<div class="pollQuestionTitle">' + question.question + '</div>';
                htmlResults += '<div class="pollQuestionType">' + (question.multipleAnswers ? '(Mai multe răspunsuri posibile)': '(Un singur răspuns posibil)') + '</div>';
                htmlResults += '<ul class="pollQuestionAnswersResults">';

                // question.answers starts at 1, and has length = nr_answers + 1
                for (var j = 0; j < answers.length; j++) {
                    htmlResults += '<li class="pollQuestionAnswerResult"><div class="answer" style="float:none;">' + answers[j] + '</div></li>';
                }
                htmlResults += '</ul>';
                htmlResults += '</div>';
            }
        }
    }
    htmlResults += '</div>';
    $("poll_questions_" + pollId).update(htmlResults);
}

function getLastUpdatedAsText(lastUpdatedTime) {
    var currentTime = new Date().getTime();
    if (lastUpdatedTime <= 0) {
        lastUpdatedTime = currentTime;
    }
    return getLastUpdatedText(currentTime, lastUpdatedTime);
}

function resizeMarginsForLCRAd() {
    var w = document.viewport.getWidth();
    var lrw = 0;
    if(w > 1000) {
        lrw = parseInt((w - 1000) / 2);
    }
    $(leftSkyAdId).style.left = (lrw - leftSkyAdWidth) + "px";
    $(leftSkyAdId).style.width = leftSkyAdWidth + "px";
    $(rightSkyAdId).style.left = (lrw + 1000) + "px";
    $(rightSkyAdId).style.width = lrw + "px";
    $(leftSkyAdId).style.display = "";
    $(rightSkyAdId).style.display = "";
}

function resizeCenterForLCRAd() {
    var mijlocArboInteractive = $('mijloc_arbointeractive');
    if(mijlocArboInteractive && mijlocArboInteractive.getHeight() > 0) {
        mijlocArboInteractive.style.paddingTop = "8px";
        mijlocArboInteractive.style.paddingBottom = "8px";
    }
}

function changeMostTab(idx, page) {
    setCookie("hn_most" + page, idx, 365, "/", ".hotnews.ro");
    $("mostContent").innerHTML = $("most" + idx).innerHTML;
    $("mostTab1").className = "celeMaiTabInactiv";
    $("mostTab2").className = "celeMaiTabInactiv";
    $("mostTab3").className = "celeMaiTabInactiv";
    $("mostTab" + idx).className = "celeMaiTabActiv";
}

function restoreMostTab(page) {
    var mostIdx = getCookie("hn_most" + page);
    var most = 1;
    if (mostIdx) {
        most = parseInt(mostIdx);
        if (isNaN(most) || most < 1 || most > 3) {
            most = 1;
        }
    }
    changeMostTab(most, page);
}

function trackMouseForPozilla(event) {
    var x = Event.pointerX(event);
    var y = Event.pointerY(event);
    if(!Position.within($('pozillaMouseOver'), x, y)) {
        $('pozillaThumbs').hide();
    } else {
        $('pozillaThumbs').show();
    }
}

/*var now = new Date();
var blockScrollCheck = false;
Event.observe( window, 'scroll', function(event) {
    if(objIsValid($('v14'))) {
        now = new Date();

        window.setTimeout("checkScrollLevel()", 1500);
    }
});*/

function checkScrollLevel() {
    // sync mechanism  - onscroll events are fired in series - to prevent handling all of them, and just one instead,
    // use this variable which is set to true for 1 sec after the first onscroll event was handled
    // (true = block any other handling of scroll event)
    if(!blockScrollCheck) {
        var now2 = new Date();
        var timeDiff = now2.getTime() - now.getTime();

        if(timeDiff > 1000) {
            var max = 14;
            for(var i = max; i > 0; i--) {
                var el = $('v' + i);
                if(objIsValid(el)) {
                    // 80 is added to add an additional space necessary for the user to see something of a particular region
                    // if no additional 80 were added - if user scrolled until seeing the top of the letters "Esential"
                    // the algorithm would've marked "esential" as visited. In 80px it's reasonable to assume the user has seen
                    // something from that section
                    if(el.viewportOffset().top + 80 < document.viewport.getDimensions().height) {
                        monitorAction("scroll_" + el.getAttribute("lang"));
                        blockScrollCheck = true;
                        window.setTimeout("blockScrollCheck = false;", 1000);
                        break;
                    }
                }
            }
        }
    }
}

var monitorAction = function(action) {
    var src = $('genericMonitoring').src;
    var questionMarkPos = src.indexOf("?");
    if(questionMarkPos > 0) {
        src = src.substring(0, questionMarkPos);
    }
    if(typeof(action) != "undefined" && action != "") {
        var newSrc = src;
        newSrc += "?" + action;
        $('genericMonitoring').src = newSrc;
    }
}

Event.observe( window, 'load', function() {
    var leatherboards = $$('.leatherboard');
    if(leatherboards && leatherboards.length > 0 && $(leatherboards[0]).getHeight() > 30) {
        var cpsd = $$('.casetaPublicitaraSusDreapta');
        if(cpsd && cpsd.length > 0) {
            cpsd[0].style.display = "";
            initQuoteGallery('3');
        }
    }
});

function submitHotreport() {
    // Check if fields are valid
    var statusMessage = $('hotreportStatusMessage');
    statusMessage.innerHTML = '';
    var photoTitle = $('hotreport_photo_title');
    var photoLocation = $('hotreport_photo_location');
    var photoDate = $('hotreport_photo_date');
    var photoDescription = $('hotreport_photo_description');
    var photoFile7 = $('uploadFile7');
    var photoFile8 = $('uploadFile8');
    var photoFile9 = $('uploadFile9');
    if (photoTitle.value.blank() || photoTitle.value.strip() == 'titlul') {
        statusMessage.innerHTML = '<span style="color:red">Nu ati completat titlul pozei!</span>';
        photoTitle.focus();
        return;
    } else if (photoLocation.value.blank() || photoLocation.value.strip() == 'locul') {
        statusMessage.innerHTML = '<span style="color:red">Nu ati completat locatia pozei!</span>';
        photoLocation.focus();
        return;
    } else if (photoDate.value.blank() || photoDate.value.strip() == 'data') {
        statusMessage.innerHTML = '<span style="color:red">Nu ati completat data pozei!</span>';
        photoDate.focus();
        return;
    } else if (photoDescription.value.blank()) {
        statusMessage.innerHTML = '<span style="color:red">Nu ati completat descrierea pozei!</span>';
        photoDescription.focus();
        return;
    } else if (photoFile7.value.blank() && photoFile8.value.blank() && photoFile9.value.blank()) {
        statusMessage.innerHTML = '<span style="color:red">Nu ati selectat niciun fisier imagine!</span>';
        photoFile7.focus();
        return;
    } else if (!($('hotreport_agree').checked)) {
        statusMessage.innerHTML = '<span style="color:red">Trebuie sa cititi si sa fiti de acord cu regulamentul!</span>';
        return;
    }
    statusMessage.innerHTML = '';
    if (objIsValid(welcomeUsername)) {
        // Already logged-in
        doSubmitHotreport();
    } else {
        var email = $('hotreport_email2');
        var password = $('hotreport_password');
        if (email.value.blank() || email.value.strip() == 'adresa e-mail') {
            statusMessage.innerHTML = '<span style="color:red">Nu ati completat adresa de e-mail!</span>';
            email.focus();
        } else {
            if (password.value.blank() || password.value.strip() == 'parola') {
                // Check if valid email
                if (!isValidEmail(email.value.strip())) {
                    statusMessage.innerHTML = '<span style="color:red">Nu ati completat o adresa de e-mail valida!</span>';
                    email.focus();
                }
                // Submit the hotreport as anonymous
                doSubmitHotreport();
            } else {
                // Try to login
                email = email.value.strip();
                password = password.value.strip();
                new Ajax.Request('/web_login_check', {
                    method: 'post',
                    postBody: 'j_username='+encodeURIComponent(email)+'&j_password='+encodeURIComponent(password)+'&ajax=true',
                    onSuccess: function(response) {
                        var msg = response.responseText;
                        if (msg.startsWith("error:")) {
                            statusMessage.innerHTML = '<span style="color:red">Login invalid! Emailul sau parola sunt gresite.</span>';
                            $('hotreport_password').focus();
                        } else if (msg.startsWith("url:")) {
                            location.href = msg.substring("url:".length);
                        } else if (msg.startsWith("success:")) {
                            // After login, submit the hotreport
                            // Replace the given nickname with the user's nickname
                            welcomeUsername = msg.substring("success:".length);
                            $('hotreport_nickname').value = welcomeUsername;
                            doSubmitHotreport();
                        } else {
                            alert('Ne pare rau, a aparut o eroare la login. Va rugam sa incercati din nou.');
                        }
                    }
                });
            }
        }
    }
}

function doSubmitHotreport() {
    $('submit_horeport').disabled = true;
    $('submit_horeport').style.color = 'gray';
//    var photoFile7 = $('uploadFile7');
    var msg = 'Se trimit fisierele...';
//    if(photoFile7.value.match(/(.)*.(wmv|wm|mpg|avi|mpeg|mov|asf|3gp|asx|mp4)/i)) {
//        msg = 'Se trimite filmul...';
//    }
    Element.update('hotreportStatusMessage', '<img src="/images/new/ajax_loading_anim_1.gif" height="16" alt=""/> ' + msg);
    $('hotreport_nickname').disabled = true;
    $('hotreport_email2').disabled = true;
    $('hotreport_password').disabled = true;
//    $('uploadFile9').disabled = true;
    $('hotreport_photo_title').disabled = true;
    $('hotreport_photo_location').disabled = true;
    $('hotreport_photo_date').disabled = true;
    $('hotreport_photo_description').disabled = true;
    $('hotreport_photo_description').readonly = true;
    $('hotreport_agree').disabled = true;
    uploadHR7();
}

function uploadHR7() {
    var photoFile7 = $('uploadFile7');
    if (photoFile7.value.blank()) {
        uploadHR8();
    } else {
        uploadMgr.startUpload('7', 'uploadHR8', /(.)*.(jpg|jpeg|gif|png|wmv|wm|mpg|flv|avi|mpeg|mov|asf|3gp|asx|mp4)/i);
    }
}

function uploadHR8() {
    var photoFile8 = $('uploadFile8');
    if (photoFile8.value.blank()) {
        uploadHR9();
    } else {
        uploadMgr.startUpload('8', 'uploadHR9', /(.)*.(jpg|jpeg|gif|png|wmv|wm|mpg|flv|avi|mpeg|mov|asf|3gp|asx|mp4)/i);
    }
}

function uploadHR9() {
    var photoFile9 = $('uploadFile9');
    if (photoFile9.value.blank()) {
        onFinishedUploadHotreport();
    } else {
        uploadMgr.startUpload('9', 'onFinishedUploadHotreport', /(.)*.(jpg|jpeg|gif|png|wmv|wm|mpg|flv|avi|mpeg|mov|asf|3gp|asx|mp4)/i);
    }
}

function onFinishedUploadHotreport() {
    var statusMessage = $('hotreportStatusMessage');
    var nickname = $('hotreport_nickname');
    var email = $('hotreport_email2');
    var photoTitle = $('hotreport_photo_title');
    var photoLocation = $('hotreport_photo_location');
    var photoDate = $('hotreport_photo_date');
    var photoDescription = $('hotreport_photo_description');
    var photoFile7 = $('uploadFile7');
    var photoFile8 = $('uploadFile8');
    var photoFile9 = $('uploadFile9');
    var parentType = null;
    var parentId = null;
    if (typeof(currentArticleId) != "undefined" && currentArticleId) {
        parentType = 1;
        parentId = parseInt(currentArticleId);
    }
    userProxy.savePicForHotreport(
        parentType,
        parentId,
        nickname.value.strip(),
        email.value.strip(),
        photoTitle.value.strip(),
        photoLocation.value.strip(),
        photoDate.value.strip(),
        photoDescription.value.strip(),
        function(resp) {
            if (resp.status != 0) {
                statusMessage.innerHTML = '<span style="color:red">' + resp.obj + '</span>';
            } else {
//                    if(photoFile.value.match(/(.)*.(wmv|wm|mpg|avi|mpeg|mov|asf|3gp|asx|mp4)/i)) {
//                        statusMessage.innerHTML = '<span style="color:darkblue">Videoclipul a fost trimis.</span>';
//                    } else {
                statusMessage.innerHTML = '<span style="color:darkblue">Fisierele au fost trimise.</span>';
//                    }
            }
            // Clear and re-enable the fields
            if (!objIsValid(welcomeUsername)) {
                nickname.disabled = false;
                email.disabled = false;
                $('hotreport_password').disabled = false;
            }
            photoFile7.value = '';
            photoFile8.value = '';
            photoFile9.value = '';
            photoTitle.value = '';
            photoDescription.value = '';
//                photoLocation.value = '';
//                photoDate.value = '';
            photoFile7.disabled = false;
            photoFile8.disabled = false;
            photoFile9.disabled = false;
            photoTitle.disabled = false;
            photoDescription.disabled = false;
            photoDescription.readonly = false;
            photoLocation.disabled = false;
            photoDate.disabled = false;
            $('hotreport_agree').disabled = false;
            $('uploadProgress9').innerHTML = '';
            $('submit_horeport').disabled = false;
            $('submit_horeport').style.color = 'black';
        }
    );
}

var hrPos = 1;
function scrollHotReporter(delta, numItems) {
    var numPages = 1 + parseInt(numItems / 9);
    var i = 0;
    if((hrPos + delta) > 0 && (hrPos + delta) <= numPages) {
        $('hr' + hrPos).hide();
        hrPos = hrPos + delta;
        $('hr' + hrPos).show();
    }
    $('hotReporterInterval').innerHTML = (1 + (hrPos - 1) * 9) + " - " + (numItems < hrPos * 9 ? numItems : hrPos * 9);
}

/**
 * Hides the anonymous panel in the right form for hotreporter and replaces it with the login panel
 */
function switchHRFormToLogin() {
    $('hotreport_nickname').hide();
    $('hotreport_email2').hide();
    $('hotreport_switchlogin').hide();
    $('loginFormHR').show();
    $('hotreport_email').value = $('hotreport_email2').value;
}

/**
 * Hides the login panel in the right form for hotreporter and replaces it with the anonymous panel
 */
function switchHRFormToAnonymous() {
    $('hotreport_nickname').show();
    $('hotreport_email2').show();
    $('hotreport_switchlogin').show();
    $('loginFormHR').hide();
    $('hotreport_email2').value = $('hotreport_email').value;
}

/**
 * Moves through HotReporter archive, showing older months
 */
function prevHRMonth() {
    var children = document.getElementsByClassName('notArrow');
    if(children) {
        var hideFirst = false;
        var showLast = false;
        for(var i = 0; i < children.length; i++) {
            if(!hideFirst && children[i].visible()) {
                if(i >= children.length - 4) {
                    break;
                }
                children[i].hide();
                hideFirst = true;
                continue;
            }
            if(hideFirst && !showLast && !children[i].visible()) {
                children[i].show();
                showLast = true;
                break;
            }
        }
    }
}

/**
 * Moves through HotReporter archive, showing newer months
 */
function nextHRMonth() {
    var children = document.getElementsByClassName('notArrow');
    if(children) {
        var showFirst = false;
        var hideLast = false;
        for(var i = children.length - 1; i >= 0; i--) {
            if(!hideLast && children[i].visible()) {
                if(i == 3) {
                    break;
                }
                children[i].hide();
                hideLast = true;
                continue;
            }
            if(hideLast && !showFirst && !children[i].visible()) {
                children[i].show();
                showFirst = true;
                break;
            }
        }
    }
}

var showBgLayer = function() {
    $('bgLayer').style.left = "0px";
    $('bgLayer').style.top = "0px";
    $('bgLayer').style.height = $(document.body).height + "px";
    $('bgLayer').show();
}

var loginAjaxFormMoved = false;
var showAjaxLoginForm = function(type) {
	showLoginBgLayer();

	if (document.viewport.getWidth() > 800){
		$('loginPopup').style.left = document.viewport.getWidth() - 800 + "px";
	}else{
		$('loginPopup').style.left = "0 px";
	}

	$('loginPopup').show();
	if (type == 2){
		showRegister()
	}else{
		showLoginHotnews();
	}
}

function showLoginTwitter(){
	$('tab_login_twitter').show();
	$('tab_login_hotnews').hide();
	$('tab_login_newsletter').hide();
	$('tab_login_sign_in').hide();
	$('tab_login_forgot_password').hide();

//	$('signup_activ').hide();
//	$('login_activ').show();
}

function showLoginHotnews(){
	$('tab_login_hotnews').show();
	$('tab_login_twitter').hide();
	$('tab_login_newsletter').hide();
	$('tab_login_sign_in').hide();
	$('tab_login_forgot_password').hide();

//	$('signup_activ').hide();
//	$('login_activ').show();
}

function showLoginNewsletter(){
	$('tab_login_hotnews').hide();
	$('tab_login_twitter').hide();
	$('tab_login_newsletter').show();
	$('tab_login_sign_in').hide();
	$('tab_login_forgot_password').hide();

//	$('signup_activ').hide();
//	$('login_activ').show();
}

function showLoginForgotPassword(){
	$('tab_login_hotnews').hide();
	$('tab_login_twitter').hide();
	$('tab_login_newsletter').hide();
	$('tab_login_forgot_password').show();
	$('tab_login_sign_in').hide();

//	$('signup_activ').hide();
//	$('login_activ').show();
}

function showLoginSignIn(){
	$('tab_login_twitter').hide();
	$('tab_login_hotnews').hide();
	$('tab_login_newsletter').hide();
	$('tab_login_forgot_password').hide();
	$('tab_login_sign_in').show();

//	$('login_activ').hide();
//	$('signup_activ').show();
}

function closeLogin(){
    $jh.colorbox.close();
}

function signupAjax() {
	var username = $('login-register-email');
	var password = $('login-register-pass');
	var confirmPassword = $('login-register-confirm-pass');
	var firstname = $('login-register-prenume');
	var lastname = $('login-register-nume');
	var nickname = $('login-register-pseudonim');
	var confirm	 = $('newsletter-na');

	if (objIsValid(username)) {
		var usernameValue = username.value.strip();

		if (usernameValue.length == 0){
			alert('Introduceti adresa de e-mail');
			username.focus();
			return;
		}
	}
	if (objIsValid(password)) {
		var passwordValue = password.value.strip();
		if (passwordValue.length == 0){
			alert('Introduceti parola');
			password.focus();
			return;
		}
		if (passwordValue.length < 5){
			alert('Parola trebuie sa aiba minim 5 caractere');
			password.focus();
			return;
		}
	}
	if (objIsValid(confirmPassword)) {
		var confirmPasswordValue = confirmPassword.value.strip();
		if ((confirmPasswordValue.length == 0) || (confirmPasswordValue != passwordValue) ){
			alert('Confirmati parola');
			confirmPassword.focus();
			return;
		}
	}
	if (objIsValid(firstname)) {
		var firstnameValue = firstname.value.strip();
		if (firstnameValue.length == 0){
			alert('Introduceti prenumele');
			firstname.focus();
			return;
		}
	}
	if (objIsValid(lastname)) {
		var lastnameValue = lastname.value.strip();
		if (lastnameValue.length == 0){
			alert('Introduceti numele');
			lastname.focus();
			return;
		}
	}
	if (objIsValid(nickname)) {
		var nicknameValue = nickname.value.strip();
		if (nicknameValue.length == 0){
			alert('Introduceti nickname-ul');
			nickname.focus();
			return;
		}
	}
	if (objIsValid(confirm) && (confirm.checked == true)) {
		userProxy.signup( username.value,  password.value,  confirmPassword.value,  firstname.value,
				lastname.value,  nickname.value,  confirm.value, "false", "false", function(response) {
			alert(response.obj);
			if(response.status == 0)
			    closeLogin();
		});
	}else {
		alert('Trebuie sa confirmati');
	}

};

function showLoginBgLayer() {
    $('bgLayer').style.left = "0px";
    $('bgLayer').style.top = "0px";
    $('bgLayer').style.height = $(document.body).getHeight() + "px";
    $('bgLayer').show();
}

/* chronometer */
var startTime = 0;
var chrStart = 0;
var chrEnd = 0;
var chrDiff = 0;
var timerID = 0;
function chrono(id, id2){
	chrEnd = new Date();
    chrDiffInMillis = chrEnd - chrStart;
    chrDiff = chrEnd - chrStart;
	chrDiff = new Date(chrDiff);
	var msec = chrDiff.getMilliseconds();
	var sec = chrDiff.getSeconds();
	var min = chrDiff.getMinutes();
	/*var hr = chrDiff.getHours();*/
	if (min < 10){
		min = "0" + min;
	}
	if (sec < 10){
		sec = "0" + sec;
	}
	if(msec < 10){
		msec = "00" +msec;
	}
	else if(msec < 100){
		msec = "0" +msec;
	}
	document.getElementById(id).innerHTML = /*hr + ":" + */min + ":" + sec + ":" + msec;
	document.getElementById(id2).value = chrDiffInMillis;
	timerID = setTimeout("chrono('" + id + "', '" + id2 + "')", 10);
}
function chronoStart(id, id2){
	chrStart = new Date();
	chrono(id, id2);
}
function chronoStop(){
	clearTimeout(timerID);
}

function startPoll(pollId) {
    if(welcomeUsername == null) {
        alert("Trebuie sa va logati inainte de a incepe sondajul");
        return;
    }
    $('poll_questions_' + pollId).show();
    $('start_poll_' + pollId).hide();

    var pollTimerIdShow = "poll_questions_" + pollId + "_timer_show";
    var pollTimerId = "poll_questions_" + pollId + "_timer";
    chronoStart(pollTimerIdShow, pollTimerId);
    $(pollTimerIdShow).show();
}

/* Returns the version of Internet Explorer or a -1 (indicating the use of another browser).*/
function getInternetExplorerVersion() {
    var rv = -1; // Return value assumes failure.
    if (navigator.appName == 'Microsoft Internet Explorer') {
        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    return rv;
}

(function () {
    /* load proxies */
    var _loadJSFile = function (filename) {
        var fileref = document.createElement('script');
        fileref.setAttribute("type", "application/javascript");
        fileref.setAttribute("src", filename);
        var head = document.getElementsByTagName('head')[0];
        head.appendChild(fileref);
    };
    var proxies = [
        "userProxy.js",
        "pollProxy.js",
        "votingProxy.js"
    ];

    proxies.each(function(proxy) {
        _loadJSFile("dwr/interface/" + proxy);
    });
    _loadJSFile("dwr/engine.js");
})();