




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">








<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
	    <title>CAS &#8211; Central Authentication Service</title>
        
           
           
                
                <link type="text/css" rel="stylesheet" href="css/cas.css" />
           
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	    <link rel="icon" href="/favicon.ico" type="image/x-icon" />
	</head>
	<body id="cas" class="fl-theme-iphone">
    <div class="flc-screenNavigator-view-container">
        <div class="fl-screenNavigator-view">
            
                
            
            <div id="content" class="fl-screenNavigator-scroll-container">


<link rel="stylesheet" type="text/css" href="http://www.hotnews.ro/css/login.css?20022012"/>
<link rel="stylesheet" type="text/css" href="http://www.hotnews.ro/css/login-fluid.css"/>


<script type="text/javascript" src="http://www.hotnews.ro/js/prototype1.6.0.2.js?20022012"></script>
<script type="text/javascript" src="http://www.hotnews.ro/js/hotnews.js?20022014"></script>
<script type="text/javascript" src="http://www.hotnews.ro/js/jquery/jquery-1.5.2.min.js?20022012"></script>

<script type="text/javascript" src="http://www.hotnews.ro/js/jquery/jquery.colorbox-min.js?20022012"></script>

<script type="text/javascript" src="http://sso.hotnews.ro/js/engine.js"></script>
<script type="text/javascript" src="http://sso.hotnews.ro/js/util.js"></script>
<script type="text/javascript" src="http://sso.hotnews.ro/js/userProxy.js"></script>


<script type="text/javascript">
var $jh = jQuery.noConflict();
</script>

<div id="hotnews-login">
      
        
          
        
      
      <div id="hotnews-login-bottom">
        <div class="hotnews-login-tab-panes">

          <div class="pane pane-login">
            <div class="pane-left">
              <p>Intra cu adresa de e-mail si parola contului de pe Hotnews.ro</p>

              <form id="loginAjaxForm" class="loginAjaxForm" action="/login" method="post">
			    

                <div class="login-form-item login-form-item-email">
                  <label for="username">E-mail:</label>
                    <input id="username" name="username" class="required" tabindex="1" class="form-text" type="text" value="" size="25" autocomplete="false"/>
                </div>
                <div class="login-form-item login-form-item-pass">
                  <label for="password">Parola:</label>
                    <input id="password" name="password" class="required" tabindex="2" class="form-text" type="password" value="" size="25" autocomplete="off"/>
                </div>
                
                  <input id="rememberMe" name="rememberMe" type="hidden" type="checkbox" value="true"/><input type="hidden" name="_rememberMe" value="on"/>
                

                <div class="login-form-submit">
		            <input type="hidden" name="lt" value="LT-96410575-2o9tOnkYPeEKavKHHrAgAcrqP5fSov" />
                    <input type="hidden" name="execution" value="e1s1" />
                    <input type="hidden" name="_eventId" value="submit" />

                    <input class="form-submit" name="submit" accesskey="l" value="Login" tabindex="4" type="submit" />
                    <a href="#" onclick="javascript:forgotPassword()" class="login-forgot-pass">Am uitat parola!</a>
                </div>

              </form>

            </div>
            <div class="pane-right">
                <form id="facebookForm" action="/login" method="post">
                    <input type="hidden" name="lt" value="LT-96410575-2o9tOnkYPeEKavKHHrAgAcrqP5fSov" />
                    <input type="hidden" name="execution" value="e1s1" />
                    <input type="hidden" name="_eventId" value="facebookSubmit" />
                    <input id="accessToken" name="accessToken" type="hidden" type="text" value=""/>
                    <input id="rememberMe" name="rememberMe" type="hidden" type="checkbox" value="true"/><input type="hidden" name="_rememberMe" value="on"/>
                </form>
                <form id="twitterForm" action="/login" method="post">
                    <input type="hidden" name="lt" value="LT-96410575-2o9tOnkYPeEKavKHHrAgAcrqP5fSov" />
                    <input type="hidden" name="execution" value="e1s1" />
                    <input type="hidden" name="_eventId" value="twitterSubmit" />
                    <input id="twitterId" name="twitterId" type="hidden" type="text" value=""/>
                    <input id="screenName" name="screenName" type="hidden" type="text" value=""/>
                    <input id="rememberMe" name="rememberMe" type="hidden" type="checkbox" value="true"/><input type="hidden" name="_rememberMe" value="on"/>
                </form>
              <p>Intra pe Hotnews.ro conectandu-te cu contul de:</p>
                
                
                
                
                
                
              <a href="#" onclick="fblogin(); return false;" class="login-facebook" title="Facebook"><img src="http://www.hotnews.ro/images/login/btn-login-facebook.png" alt="Facebook" /></a>
              <a href="#" id="twitter-signin-btn" class="login-twitter" title="Twitter"><img src="http://www.hotnews.ro/images/login/btn-login-twitter.png" alt="Twitter" /></a>
            </div>
          </div>

        </div>
      </div>
    </div>

<script type="text/javascript">
    window.onload = function() {
		attachEventsToLogin();
	}
</script>
<!-- Facebook scripts -->
<div id="fb-root"></div>

<script src="http://platform.twitter.com/anywhere.js?id=VuyCQK7Vw9omzxx7YZKygg&v=1" type="text/javascript"></script>
<script type="text/javascript">
   //initializing API
      window.fbAsyncInit = function() {
             FB.init({appId: '840366639363095', channelUrl: '//hotnews.ro/', status: true, cookie: true,
                  xfbml: true, oauth: true});
             if(typeof(welcomeUsername) == "undefined" || welcomeUsername==null || welcomeUsername=="") {
                 fbGetLoginStatus();
             }
          };

            (function(d){
              var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
              js = d.createElement('script'); js.id = id; js.async = true;
              js.src = "//connect.facebook.net/en_US/all.js";
              d.getElementsByTagName('head')[0].appendChild(js);
            }(document));



       function fbGetLoginStatus(){
           if(getCookie("hn_check_fb") != "true") {
               return;
           }
           FB.getLoginStatus(function(response) {
             if (response.authResponse) {
               fbAjaxLogin(response);
             } else {
               // no user session available, someone you dont know
             }
           });
       }

       //your fb login function
       function fblogin() {
          FB.getLoginStatus(function(response) {
             if (response.authResponse) {
               submitFacebook(response.authResponse);
             } else {
               FB.login(function(response) {
                 if (response.authResponse) {
                     submitFacebook(response.authResponse);
                 } else {
                     closeLogin();
                 }
                }, {});
             }
           });

       }

       function fbLogout(){
           FB.logout(function(response) {
             // user is now logged out
           });
       }

   function submitFacebook(authResponse) {
       $jh('#accessToken').val(authResponse.accessToken);
       $jh('#facebookForm').submit();
   }

   function submitTwitter(user) {
       $jh('#twitterId').val(user.screenName);
       $jh('#screenName').val(user.name);
       $jh('#twitterForm').submit();
   }



<!-- Twitter scripts -->

 twttr.anywhere(function (T) {
      if(typeof(welcomeUsername) == "undefined" || welcomeUsername==null || welcomeUsername=="") {
         if (T.isConnected()) {
           if(getCookie("hn_check_tw") == "true") {
               submitTwitter(T.currentUser);
           }
         }
      }
      document.getElementById("twitter-signin-btn").onclick = function () {
        T.signIn();
      };
      T.bind("authComplete", function (e, user) {
          submitTwitter(user);
      });
  });

  function twLogout() {
      twttr.anywhere.signOut();
  }

</script>