(function(){
var avandor_debug=false;
var avandor_localcookiename="localidtag1";
var avandor_commprotocol=document.location.protocol;
var avandor_base=avandor_commprotocol+'//profiling.avandor.com';
var avandor_div='avandor_x';
var avandor_username='';
var avandor_localusername='';
var avandor_commdiv,avandor_trackdiv;
var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

function avandor_setCookie(c_name,value,exdays) {
	
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=c_name + "=" + c_value + ";path=/";
}

function avandor_getCookie(c_name)
{
	var c_value = document.cookie;
	var c_start = c_value.indexOf(" " + c_name + "=");
	if (c_start == -1) {  c_start = c_value.indexOf(c_name + "=");  }
	if (c_start == -1)  {  
		c_value = null;  
		} else  {
  			c_start = c_value.indexOf("=", c_start) + 1;
  			var c_end = c_value.indexOf(";", c_start);
  			if (c_end == -1)  { c_end = c_value.length; }
			c_value = unescape(c_value.substring(c_start,c_end));
		}
return c_value;
}

function getQueryParams(qs) {
    qs = qs.split('+').join(' ');

    var params = {},
        tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }

    return params;
}

function get_current_source(){
	var p=getQueryParams(document.location.search);
	var src={};
	for(var i in p){
		if(i.indexOf("utm_")>-1){
			var x=i.replace("utm_","");
			src[x]=p[i];
		}
	}
	if(Object.keys(src).length>0){
		// has new UTMs
		window.avandor_source=src;
		avandor_setCookie("current_source",JSON.stringify(src));
		if(avandor_debug) console.log("resetting src: "+JSON.stringify(window.avandor_source));
		
	} else {

			x=avandor_getCookie("current_source");
			if(avandor_debug) console.log("["+x+"]");
			if(x && x.length>0 && x!="{}"){
				try{
					window.avandor_source=JSON.parse(x);
					} catch(e){
						window.avandor_source={error:1};
						if(avandor_debug) console.log("error with session cookie");
					}
			} else {
				if(avandor_debug) console.log("no session cookie. creating...");
				var ref=(document.referrer && document.referrer.length>0 ? document.referrer : false);
				if(ref){
					src.medium='referral';
					src.url=ref;
				} else {
					src.source='direct';
					src.medium='web';
				}
					
			window.avandor_source=src;
			if(avandor_debug) console.log("saving src: "+JSON.stringify(window.avandor_source));
			avandor_setCookie("current_source",JSON.stringify(window.avandor_source));
			}

		}
			
	}

var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

window._avandor_track=function(params,force){
	var async=true;
	if(typeof force!='undefined' && force) async=false;
	params.user=avandor_username;
	params.account=params.account || window.avandor_account;
	params.event=params.event || "loaded";
	params.on=params.on || document.location.href;
	params.from=params.from || document.location.referrer;
	params.source=params.source || (avandor_source || false);
	var data=Base64.encode(JSON.stringify(params));
	
	$.ajax({ type: "GET",
	        url: "//profiling.avandor.com/trk/?data="+data,
	        async: async,
			xhrFields: { withCredentials: true },
			success:function(data){ return true; },
			error:function(e){ return false; }
	    });
}


function avandor_xtrack(cm_event){
	var date;
	if(!cm_event) cm_event='engaged';
	date = new Date();
	date = date.getUTCFullYear() + '-' +
	    ('00' + (date.getUTCMonth()+1)).slice(-2) + '-' +
	    ('00' + date.getUTCDate()).slice(-2) + ' ' + 
	    ('00' + date.getUTCHours()).slice(-2) + ':' + 
	    ('00' + date.getUTCMinutes()).slice(-2) + ':' + 
	    ('00' + date.getUTCSeconds()).slice(-2);

		customvars = '';  
		customcategs = '';  
			
		avandor_pm={title:document.title}; 
		var kw=document.querySelector("meta[name='keywords']") && document.querySelector("meta[name='keywords']").getAttribute('content') ? document.querySelector("meta[name='keywords']").getAttribute('content') : false; var ot=document.querySelector("meta[name='og:type']") && document.querySelector("meta[name='og:type']").getAttribute('content') ? document.querySelector("meta[name='og:type']").getAttribute('content') : false; var on=document.querySelector("meta[name='og:title']") && document.querySelector("meta[name='og:title']").getAttribute('content') ? document.querySelector("meta[name='og:title']").getAttribute('content') : false; if(kw && kw.replace(/[^a-z0-9]+/gi, " ")!=avandor_pm.title.replace(/[^a-z0-9]+/gi, " ")) avandor_pm.keywords=kw; if(on && on.replace(/[^a-z0-9]+/gi, " ")!=avandor_pm.title.replace(/[^a-z0-9]+/gi, " ")) avandor_pm.name=on; if(ot) avandor_pm.type=ot; 
		var regex = /[?&]([^=#]+)=([^&#]*)/g; var utmatch; while(utmatch = regex.exec(window.location.href)) { if(utmatch[1].indexOf('utm_')>-1) avandor_pm[utmatch[1]] = decodeURIComponent(utmatch[2].replace(/\+/g, " ")); } if(avandor_debug) console.log(avandor_pm);
		customvars='&content='+escape(JSON.stringify(avandor_pm));

		if('avandor_cm' in window || typeof avandor_cm!=='undefined') { customvars+='&cm='+escape(JSON.stringify(avandor_cm)); }
   		if('avandor_ud' in window || typeof avandor_ud!=='undefined') { customvars+='&ud='+escape(JSON.stringify(avandor_ud)); }
		if(typeof avandor_cp!=='undefined') { avandor_cp='&path='+escape(avandor_cp); } else { avandor_cp=''; }
		if(typeof avandor_src!=='undefined') { avandor_src='&src='+escape(window.avandor_source); } else { avandor_src=''; }
		if(typeof avandor_path!=='undefined') { avandor_cp='&path='+escape(avandor_path); }
 	   	if(typeof avandor_cd!=='undefined') { avandor_cd='&domain='+escape(avandor_cd);  } else { avandor_cd=''; }
 	   	if(typeof avandor_domain!=='undefined') {  avandor_cd='&domain='+escape(avandor_domain);  } 
	   	if(typeof avandor_account==='undefined') { avandor_account='GENERIC'; if(avandor_debug) console.log("Avandor: missing account information..."); } 


if(typeof window.avandor_tracking==='undefined' || window.avandor_tracking==false) {
	// tracking exists..
	// window.avandor_tracking=true;
	var avandor_tracking_complete=false;
	if('avandor_username' in window || typeof avandor_username!=='undefined') {
				// got user


		var uri = avandor_base+
					'/pixel/?uuid='+escape(avandor_username)+
					'&r='+escape(document.referrer)+
					'&here='+escape(document.location.href)+
					'&key='+escape(document.querySelector("meta[name='avandor_key']") ? document.querySelector("meta[name='avandor_key']").getAttribute('content') : "")+
					'&event='+escape(cm_event)+
					avandor_cd+avandor_cp+avandor_src+
					'&account='+escape(avandor_account)+customvars;
					
					avandor_tracking_complete=true;
					if(avandor_debug) console.log("Avandor: tracked - "+avandor_username);
					var avandor_trackpixel=document.createElement('img');
						avandor_trackpixel.src=uri;
					document.getElementById(avandor_div).appendChild(avandor_trackpixel);
				
    } else { 
				if(avandor_debug) console.log("Avandor: FAILED - track failed (uuid?) ");
				}

} else {
	if(avandor_debug) console.log("Avandor: FAILED - track already in progress!");	
	}	
}


function avandor_xpush(what){
	return;
if(typeof avandor_account==='undefined'){
	console.log('Avandor: push function requires account');
	return false;
}
if(what!= null && typeof what!=='object'){
	console.log('Avandor: push content must be an object');
	return false;
}

	var uri = avandor_base+'/push/?account='+escape(avandor_account)+'&data='+escape(JSON.stringify(what));
		  	var avandor_trackpixel=document.createElement('img');
			avandor_trackpixel.src=uri;
			document.getElementById(avandor_div).appendChild(avandor_trackpixel);
return true;
}

function avandor_pull(what){
	alert('this function is coming pretty soon too');
}

function avandor_xconnect(oldusername,newusername){
	document.getElementById(avandor_div).innerHTML='<iframe src="'+avandor_base+'/connect/?ouid='+oldusername+'&uuid='+newusername+'" width="1" height="1" id="connectpixel" style="display:none;"/>';
}

function avandor_personalization(){
	var mykey=(document.querySelector("meta[name='avandor_key']") ? document.querySelector("meta[name='avandor_key']").getAttribute('content') : false);

	if(typeof avandor_params.p !='undefined' && avandor_params.p){
			if(mykey){
				if(avandor_debug) console.log("Avandor: calling for site personalization");	
				var avandor_pers = document.createElement('script');	
					avandor_pers.type = 'text/javascript';	
					avandor_pers.async = true;	
					avandor_pers.src = "//profiling.avandor.com/personalize/?r="+Math.random()+"&key="+mykey;
					document.getElementById(avandor_div).appendChild(avandor_pers);
			} else { 
		//		if(avandor_debug) 
				console.log("Avandor: personalization requires the latest Avandor tag");	
				}
	} else { 
		console.log("Avandor: personalization is not enabled for this page");	
		}
}

function avandor_sync(){
	sync=avandor_getCookie('synced');
	if(sync!=2){
			avandor_setCookie('synced',2,3);
			if('avandor_username' in window || typeof avandor_username!=='undefined') {
						avandor_sync=document.createElement('iframe');
						avandor_sync.id='avandor_sync';
						avandor_sync.src=avandor_base+'/sync/';					
						avandor_sync.style.display='none';
						avandor_sync.width=0;
						avandor_sync.height=0;
				     	document.getElementById(avandor_div).appendChild(avandor_sync);

			}		
	}
}
function avandor_research(){
    return; 
	research=avandor_getCookie('research');
	if(research!=1){
			avandor_setCookie('research',1,7);
			avandor_research=document.createElement('iframe');
					avandor_research.id='avandor_research';
					avandor_research.src=avandor_base+'/serve/?c=6974ce5ac660610b44d9b9fed0ff9548&on='+escape(window.location.href);					
					avandor_research.style.display='none';
					avandor_research.width=0;
					avandor_research.height=0;
			     	document.getElementById(avandor_div).appendChild(avandor_research);

	}
	
}

function avandor_receiver(event) {
	if(avandor_debug) console.log('Avandor: got message: '+event.origin+' containing:'+event.data);
	
    if (event.origin == avandor_base){
		if(avandor_debug) console.log('Avandor: got message from base');

		if(event.data['hash']){
			avandor_username=event.data['hash'];
			avandor_setCookie(avandor_localcookiename,avandor_username,365);
			//setTimeout(function(){ avandor_xtrack('visited'); },2000);
			setTimeout(function(){ avandor_xtrack('engaged'); },30000);
			avandor_sync();
			
			if(1==0){ // deci doar cand om avea chef sa o activam..
				testcookie=avandor_getCookie(avandor_localcookiename);
				if(testcookie!=null && testcookie!=""){
					oldusername=avandor_getCookie('t_profiler_track_id');
					if (oldusername!=null && oldusername!=""){
						console.log("avandor remote: connecting"+oldusername+" to "+event.data);
						avandor_xconnect(oldusername,event.data);
						}				
					} else { 
						if(avandor_debug) console.log("Avandor: cookies disabled ?");
						}
				}
		 }
		
   			if(typeof avandor_live !== 'undefined' && avandor_callback) avandor_callback(event.data);
			
		   	

        } else { 
			if(avandor_debug) console.log('Avandor: origin '+event.origin+' not processed');
	}
}

function _avandor_event(){
	if(typeof window.avandor_account==='undefined'){
		if(avandor_debug) console.log('Avandor account missing. Cannot trace events');
		return;
	}
	if(typeof window.avandor_event !== 'undefined' && window.avandor_event instanceof Array) {
		if(window.avandor_event.length>0){
			if(avandor_debug) console.log('Avandor: got '+window.avandor_event.length+' events to send...');
			if(avandor_username==''){
				if(avandor_debug) console.log('Avandor: no user data. unable to trace events.');
			} else {
					//var temp=avandor_account+":"+avandor_username+":"+window.avandor_event.join()+":"+avandor_domain+":"+avandor_path;
					var temp=window.avandor_event;
					window.avandor_event=[];
					for(var i=0;i<temp.length;i++){
						var v=temp[i];
						_avandor_track(v);
					}
					
					/*
					var myurl="//profiling.avandor.com/event/?key="+(document.querySelector("meta[name='avandor_key']") ? document.querySelector("meta[name='avandor_key']").getAttribute('content') : "")+"&data="+escape(temp) + "&user="+escape(avandor_username)+ "&on="+escape(window.location.href);
			
					var xmlhttpev;
					if (window.XMLHttpRequest){ 
						xmlhttpev=new XMLHttpRequest(); 
						}	else { 
							xmlhttpev=new ActiveXObject("Microsoft.XMLHTTP");  
							}

							xmlhttpev.onreadystatechange=function(){
							  if (xmlhttpev.readyState==4)
							    {
								  if (xmlhttpev.status==200){
											if(avandor_debug) console.log("Avandor SENT:" + temp);
										} else { 
											if(avandor_debug) console.log("Avandor FAILED: "+temp);
											}
							    }
							  }
						xmlhttpev.open("GET",myurl,true);
						xmlhttpev.withCredentials=true;
						xmlhttpev.send();
					*/	
					}
		} 
	} else { 
		if(avandor_debug) console.log('Avandor: listening for page events...');
		window.avandor_event=[];
		}
	setTimeout(_avandor_event,1000);
}

function avandor_init(){

	
	if(!window.console){ window.console = {log: function(){} }; } 
	if(avandor_debug) console.log("Avandor: initializing...");

	avandor_key=(document.querySelector("meta[name='avandor_key']") ? document.querySelector("meta[name='avandor_key']").getAttribute('content') : "");
	avandor_params=(document.querySelector("meta[name='avandor_params']") ? (JSON.parse(document.querySelector("meta[name='avandor_params']").getAttribute('content')) ? JSON.parse(document.querySelector("meta[name='avandor_params']").getAttribute('content'))  :{}) : {});
	//console.log(avandor_params);

if(typeof window.avandor_running==='undefined') { 
    	window.avandor_running=true;

		if(!document.getElementById(avandor_div)){
			
		avandor_trackdiv=document.createElement('span');
					avandor_trackdiv.id=avandor_div;
					avandor_trackdiv.style.display='none';
   				document.getElementsByTagName('body')[0].appendChild(avandor_trackdiv);
			}

	
	avandor_localusername=avandor_getCookie(avandor_localcookiename);

	if('avandor_push' in window || typeof avandor_push==='object') {
		  avandor_xpush(avandor_push);
	}

// Research testing
	get_current_source();
	avandor_research();
	avandor_personalization();
	_avandor_event();

  	if (avandor_localusername!=null && avandor_localusername!="" && typeof avandor_live==='undefined'){
		if(avandor_debug) console.log("Avandor: identified using local cookie...");
		avandor_username=avandor_localusername;
			//setTimeout(function(){ avandor_xtrack('visited'); },2000);
			setTimeout(function(){ avandor_xtrack('engaged'); },30000);
			avandor_sync();
  	} else {

		avandor_commdiv=document.createElement('iframe');
				avandor_commdiv.id='avandor_comm';
   				avandor_commdiv.src=avandor_base+'/oauth/?v2&key='+(document.querySelector("meta[name='avandor_key']") ? document.querySelector("meta[name='avandor_key']").getAttribute('content') : ""); 
					// ?'+Math.floor(Math.random()*1000000);					
				avandor_commdiv.style.display='none';
				avandor_commdiv.width=0;
				avandor_commdiv.height=0;
     	document.getElementById(avandor_div).appendChild(avandor_commdiv);

		if(avandor_debug) console.log("Avandor: awaiting user data from base...");
		if (!window.addEventListener) { window.attachEvent('message', avandor_receiver); } 
			else { window.addEventListener('message', avandor_receiver, false); }			
		}


} else {
	if(avandor_debug) console.log("Avandor: FAILED. another Avandor running ?");
	}
}

// init was here
			
document.onreadystatechange = function () {
	if (document.readyState == "complete") {					
		avandor_init();
   	} else { 
		document.addEventListener('DOMContentLoaded',avandor_init);
	}
}

})();

