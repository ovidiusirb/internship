
// Provide a default path to dwr.engine
if (dwr == null) var dwr = {};
if (dwr.engine == null) dwr.engine = {};
if (DWREngine == null) var DWREngine = dwr.engine;

if (userProxy == null) var userProxy = {};
userProxy._path = '/dwr';
userProxy.logout = function(callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'logout', callback);
}
userProxy.savePic = function(callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'savePic', callback);
}
userProxy.savePicForCoverage = function(p0, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'savePicForCoverage', p0, callback);
}
userProxy.savePicForHotreport = function(p0, p1, p2, p3, p4, p5, p6, p7, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'savePicForHotreport', p0, p1, p2, p3, p4, p5, p6, p7, callback);
}
userProxy.checkPic = function(p0, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'checkPic', p0, callback);
}
userProxy.checkPicForCoverage = function(p0, p1, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'checkPicForCoverage', p0, p1, callback);
}
userProxy.saveVideo = function(p0, p1, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'saveVideo', p0, p1, callback);
}
userProxy.saveVideoForCoverage = function(p0, p1, p2, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'saveVideoForCoverage', p0, p1, p2, callback);
}
userProxy.saveCoverageWithTag = function(p0, p1, p2, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'saveCoverageWithTag', p0, p1, p2, callback);
}
userProxy.saveCoverage = function(p0, p1, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'saveCoverage', p0, p1, callback);
}
userProxy.removePic = function(p0, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'removePic', p0, callback);
}
userProxy.removeCoverage = function(p0, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'removeCoverage', p0, callback);
}
userProxy.changeTitle = function(p0, p1, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'changeTitle', p0, p1, callback);
}
userProxy.changeCoverageTitle = function(p0, p1, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'changeCoverageTitle', p0, p1, callback);
}
userProxy.changeCoverageDescription = function(p0, p1, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'changeCoverageDescription', p0, p1, callback);
}
userProxy.getRates = function(p0, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'getRates', p0, callback);
}
userProxy.convertCurrency = function(p0, p1, p2, p3, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'convertCurrency', p0, p1, p2, p3, callback);
}
userProxy.calculateVat = function(p0, p1, p2, p3, p4, p5, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'calculateVat', p0, p1, p2, p3, p4, p5, callback);
}
userProxy.forgotPassword = function(p0, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'forgotPassword', p0, callback);
}
userProxy.subscribeToNewsletter = function(p0, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'subscribeToNewsletter', p0, callback);
}
userProxy.subscribeToNewsletterBreakingNews = function(p0, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'subscribeToNewsletterBreakingNews', p0, callback);
}
userProxy.subscribeToEspanolNewsletter = function(p0, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'subscribeToEspanolNewsletter', p0, callback);
}
userProxy.subscribeToStartupCafeNewsletter = function(p0, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'subscribeToStartupCafeNewsletter', p0, callback);
}
userProxy.saveUserInputOverlayOnSession = function(p0, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'saveUserInputOverlayOnSession', p0, callback);
}
userProxy.getUserInputOverlayFromSession = function(callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'getUserInputOverlayFromSession', callback);
}
userProxy.removeUserInputOverlayFromSession = function(callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'removeUserInputOverlayFromSession', callback);
}
userProxy.saveUserInputOverlay = function(p0, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'saveUserInputOverlay', p0, callback);
}
userProxy.reorderPics = function(p0, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'reorderPics', p0, callback);
}
userProxy.reorderCoveragePics = function(p0, p1, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'reorderCoveragePics', p0, p1, callback);
}
userProxy.toggleArticle = function(p0, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'toggleArticle', p0, callback);
}
userProxy.signup = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'signup', p0, p1, p2, p3, p4, p5, p6, p7, p8, callback);
}
userProxy.checkUserLoggedIn = function(callback) {
  dwr.engine._execute(userProxy._path, 'userProxy', 'checkUserLoggedIn', callback);
}
