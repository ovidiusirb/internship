
// Provide a default path to dwr.engine
if (dwr == null) var dwr = {};
if (dwr.engine == null) dwr.engine = {};
if (DWREngine == null) var DWREngine = dwr.engine;

if (pollProxy == null) var pollProxy = {};
pollProxy._path = '/dwr';
pollProxy.submitPollAnswers = function(p0, p1, callback) {
  dwr.engine._execute(pollProxy._path, 'pollProxy', 'submitPollAnswers', p0, p1, callback);
}
pollProxy.getPollResults = function(p0, callback) {
  dwr.engine._execute(pollProxy._path, 'pollProxy', 'getPollResults', p0, callback);
}
