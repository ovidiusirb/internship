var TWAGORAINARTICLE=TWAGORAINARTICLE||function(){

    
        return{
           	getCStag:function(){
                return [
					{ /* Homepage 300x250 */
							selector:'#pageBodyx > div > div.contentFather > div > div.right_side > div:nth-child(1)',
							code: `<div id="801891" style="width:300px;height:250px; margin:0 auto !important;"></div>
									<script type="text/javascript">
									var Criteo = Criteo || {};
									Criteo.events = Criteo.events || [];
									Criteo.events.push(function() {
										Criteo.DisplayAcceptableAdIfAdblocked({
											zoneid: 801891,
											containerid: "801891"
										});
									});
								</script>`
						},
						{ /* Homepage 300x600 */
							selector:'.ultimaOra',
							code: `<div id="839955" style="width:300px;height:250px; margin:0 auto !important; margin-top:5px;"></div>
									<script type="text/javascript">
									var Criteo = Criteo || {};
									Criteo.events = Criteo.events || [];
									Criteo.events.push(function() {
										Criteo.DisplayAcceptableAdIfAdblocked({
											zoneid: 839955,
											containerid: "839955"
										});
									});
								</script>`
						}
						,{  /*ROS 300x600*/
							selector:'#pageBodyx > div > div.contentFather > div > div.right_side > div:nth-child(3)',
							code: `<div id="839964" style="width:300px;height:250px; margin:0 auto !important;"></div>
									<script type="text/javascript">
									var Criteo = Criteo || {};
									Criteo.events = Criteo.events || [];
									Criteo.events.push(function() {
										Criteo.DisplayAcceptableAdIfAdblocked({
											zoneid: 839964,
											containerid: "839964"
										});
									});
								</script>`
						} 
					]
			}         
        }

    }();
	
	(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
		js.async = true;
        js.src = "//static.criteo.net/js/ld/publishertag.js";
        fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'scr-ctag'));

    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://projectagora.s3.amazonaws.com/refactor/tw_agora_inarticle.2.min.js";
        fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'pa-tag'));
    