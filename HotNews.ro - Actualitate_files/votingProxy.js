
// Provide a default path to dwr.engine
if (dwr == null) var dwr = {};
if (dwr.engine == null) dwr.engine = {};
if (DWREngine == null) var DWREngine = dwr.engine;

if (votingProxy == null) var votingProxy = {};
votingProxy._path = '/dwr';
votingProxy.voteForObject = function(p0, p1, p2, callback) {
  dwr.engine._execute(votingProxy._path, 'votingProxy', 'voteForObject', p0, p1, p2, callback);
}
votingProxy.moderateContestItemForFinale = function(p0, p1, callback) {
  dwr.engine._execute(votingProxy._path, 'votingProxy', 'moderateContestItemForFinale', p0, p1, callback);
}
