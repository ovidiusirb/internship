def compress(list):
    cnt = 1
    rlist=[]
    try:
        for i in range(0,len(list),1):
            if list[i]==list[i+1]:
                cnt+=1
            if list[i]!=list[i+1]:
                rlist.append(cnt)
                rlist.append(list[i])
                cnt=1
    except IndexError:
        rlist.append(cnt)
        rlist.append(list[-1])
    return rlist

def decompress(list):
    rlist = []
    r = range(0,len(list)-1,1)
    for i in r:
        if i %2==0:
            cnt = list[i]
            while cnt != 0:
                rlist.append(list[i+1])
                cnt-=1
    return rlist


def testCompress():
    list = [0,0,0,1,1,7,0,0,0]



print(compress([0,0,0,1,1,7,0,0,0]))
print(decompress([3, 0, 2, 1, 1, 7, 3, 0]))